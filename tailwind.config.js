/** @type {plugin | ((options?: Partial<{className: string; target: "modern" | "legacy"}>) => {handler: () => void})} */
const typography = require("@tailwindcss/typography")

module.exports = {
  content: ["./projects/**/*.{html,ts}"],
  theme: {
    extend: {},
  },
  plugins: [
      typography(function({ addUtilities, addComponents, e, config }) {
      })
  ],
}

