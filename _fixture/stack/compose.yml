version: "3.1"

volumes:
  traefik_letsencrypt: {}
  crowdsec_data: {}
  crowdsec_config: {}
  pgdata: {}

services:
  traefik:
    image: "traefik:v3.0"
    container_name: "traefik"
    restart: unless-stopped
    ports:
      - "80:80"
      - "443:443"
    volumes:
      - /var/run/docker.sock:/var/run/docker.sock:ro
      - ./containers/traefik/logs:/var/log/traefik
      - ./config/traefik.config.yml:/config.yml:ro
      - traefik_letsencrypt:/letsencrypt
    command:
      - "--api=false"
      - "--ping"
      - "--providers.file.filename=/config.yml"
      - "--providers.docker=true"
      - "--providers.docker.watch=true"
      - "--providers.docker.exposedbydefault=false"
      - "--entrypoints.web.address=:80"
      - "--entrypoints.web.http.redirections.entryPoint.to=websecure"
      - "--entrypoints.web.http.redirections.entryPoint.scheme=https"
      - "--entrypoints.websecure.address=:443"
      - "--entrypoints.websecure.http.tls=true"
      - "--entrypoints.websecure.http.tls.certResolver=shresolver"
      - "--certificatesresolvers.shresolver.acme.httpchallenge=true"
      - "--certificatesresolvers.shresolver.acme.httpchallenge.entrypoint=web"
      - "--certificatesresolvers.shresolver.acme.email=admin@casavina.de"
      - "--certificatesresolvers.shresolver.acme.storage=/letsencrypt/acme.json"
      - "--log.level=INFO"
      - "--log.filePath=/var/log/traefik/traefik.log"
      - "--accesslog=true"
      - "--accesslog.filepath=/var/log/traefik/access.log"

  crowdsec:
    image: crowdsecurity/crowdsec:v1.6.0-1-slim
    container_name: crowdsec
    restart: unless-stopped
    environment:
      GID: 1000
      COLLECTIONS: crowdsecurity/linux crowdsecurity/traefik
    volumes:
      - ./containers/traefik/logs:/var/log/traefik:ro
      - ./config/crowdsec.acquis.yaml:/etc/crowdsec/acquis.yml:ro
      - crowdsec_data:/var/lib/crowdsec/data
      - crowdsec_config:/etc/crowdsec

  bouncer-traefik:
    image: fbonalair/traefik-crowdsec-bouncer:0.5
    container_name: crowdsec-bouncer-traefik
    restart: unless-stopped
    environment:
      CROWDSEC_AGENT_HOST: crowdsec:8080
      GIN_MODE: release
    env_file:
      - ./.env

  ntfy:
    image: binwiederhier/ntfy:v2.8.0
    container_name: ntfy
    restart: unless-stopped
    command: serve
    volumes:
      - ./ntfy/cache:/var/cache/ntfy
      - ./ntfy/user:/var/lib/ntfy
      - ./config/ntfy.config.yml:/etc/ntfy/server.yml:ro
    labels:
      - "traefik.enable=true"
      - "traefik.http.routers.ntfy-secure.entrypoints=websecure"
      - "traefik.http.routers.ntfy-secure.rule=Host(`ntfy.casavina.de`)"
      - "traefik.http.routers.ntfy-secure.tls=true"
      - "traefik.http.routers.ntfy-secure.tls.certresolver=shresolver"
      - "traefik.http.routers.ntfy-secure.service=ntfy"
      - "traefik.http.routers.ntfy-secure.middlewares=chain-no-auth@file"
      - "traefik.http.services.ntfy.loadbalancer.server.port=80"

  db:
    image: postgres:15.4
    container_name: casavina-db
    restart: unless-stopped
    env_file:
      - ./passwords.env
    volumes:
      - pgdata:/var/lib/postgresql/data

  api:
    image: docker.io/library/backend:1.0.3
    container_name: casavina-api
    restart: unless-stopped
    environment:
      DB_HOST: jdbc:postgresql://db:5432/postgres
      ADMIN_USER: cviadm
      PUBLIC_KEY: /opt/casavina/public.pem
      PRIVATE_KEY: /opt/casavina/private.pem
      SPRING_PROFILES_ACTIVE: prod
    env_file:
      - ./passwords.env
    depends_on:
      - db
    volumes:
      - /opt/casavina:/opt/casavina:ro
    labels:
      - "traefik.enable=true"
      - "traefik.http.routers.api-secure.entrypoints=websecure"
      - "traefik.http.routers.api-secure.rule=Host(`api.casavina.de`)"
      - "traefik.http.routers.api-secure.tls=true"
      - "traefik.http.routers.api-secure.tls.certresolver=shresolver"
      - "traefik.http.routers.api-secure.service=api"
      - "traefik.http.routers.api-secure.middlewares=chain-no-auth@file"
      - "traefik.http.services.api.loadbalancer.server.port=8080"

  admin-page:
    image: registry.gitlab.com/silentflick/casavina/admin-page:1.0.4
    container_name: casavina-admin
    restart: unless-stopped
    labels:
      - "traefik.enable=true"
      - "traefik.http.routers.admin-secure.entrypoints=websecure"
      - "traefik.http.routers.admin-secure.rule=Host(`internal.casavina.de`)"
      - "traefik.http.routers.admin-secure.tls=true"
      - "traefik.http.routers.admin-secure.tls.certresolver=shresolver"
      - "traefik.http.routers.admin-secure.service=admin"
      - "traefik.http.routers.admin-secure.middlewares=chain-no-auth@file"
      - "traefik.http.services.admin.loadbalancer.server.port=80"

  landing-page-ssr:
    image: registry.gitlab.com/silentflick/casavina/landing-page:1.0.4
    container_name: casavina-landing
    restart: unless-stopped
    labels:
      - "traefik.enable=true"
      - "traefik.http.routers.landing-secure.entrypoints=websecure"
      - "traefik.http.routers.landing-secure.rule=Host(`casavina.de`)"
      - "traefik.http.routers.landing-secure.tls=true"
      - "traefik.http.routers.landing-secure.tls.certresolver=shresolver"
      - "traefik.http.routers.landing-secure.service=landing"
      - "traefik.http.routers.landing-secure.middlewares=chain-no-auth@file"
      - "traefik.http.services.landing.loadbalancer.server.port=4000"


