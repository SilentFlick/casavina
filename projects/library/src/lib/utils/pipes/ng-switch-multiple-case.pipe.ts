import {Pipe, PipeTransform} from "@angular/core";

@Pipe({
	name: "ngSwitchMultipleCase",
	standalone: true
})
export class NgSwitchMultipleCasePipe implements PipeTransform {
	transform(value: string, ...args: string[]): string {
		return args.includes(value) ? value : "";
	}
}
