import {provideStoreDevtools} from "@ngrx/store-devtools";

export const environment = {
	production: false,
	apiUrl: "",
	providers: [
		provideStoreDevtools({
			maxAge: 25
		})
	]
};
