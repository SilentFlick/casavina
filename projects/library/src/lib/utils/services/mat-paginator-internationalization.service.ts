import {Injectable} from "@angular/core";
import {MatPaginatorIntl} from "@angular/material/paginator";
import {Subject} from "rxjs";

@Injectable()
export class MatPaginatorInternationalizationService
	implements MatPaginatorIntl {
	public changes = new Subject<void>();

	public firstPageLabel = "Erste Seit";
	public itemsPerPageLabel = "Elemente pro Seite";
	public lastPageLabel = "Letzte Seite";

	public nextPageLabel = "Nächste Seite";
	public previousPageLabel = "Vorherige Seite";
	public ofLabel = "von";

	public getRangeLabel(page: number, pageSize: number, length: number): string {
		if (length === 0) {
			return `0 of ${length}`;
		}
		const startIndex = page * pageSize;
		const endIndex =
			startIndex < length
				? Math.min(startIndex + pageSize, length)
				: startIndex + pageSize;
		return `${startIndex + 1} - ${endIndex} ${this.ofLabel} ${length}`;
	}
}
