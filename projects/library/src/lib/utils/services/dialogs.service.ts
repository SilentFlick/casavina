import {Injectable} from "@angular/core";
import {AsyncValidatorFn, ValidatorFn} from "@angular/forms";
import {DialogFormComponent} from "@casavina/ui/dialog-form/dialog-form.component";
import {InputModel} from "@casavina/ui/dynamic-components/form/models/input-model";
import {DialogService} from "primeng/dynamicdialog";
import {take} from "rxjs";

@Injectable({
	providedIn: "root"
})
export class DialogsService {
	constructor(private dialogService: DialogService) {
	}

	public openFormDialog(
		inputModel: InputModel,
		title?: string,
		groupValidators?: (ValidatorFn | AsyncValidatorFn)[]
	) {
		const dialogRef = this.dialogService.open(DialogFormComponent, {
			header: title,
			contentStyle: {overflow: "auto"},
			baseZIndex: 10000,
			modal: true,
			maximizable: true,
			resizable: true,
			draggable: true,
			data: {
				inputModel: inputModel,
				groupValidators: groupValidators
			}
		});
		return dialogRef.onClose.pipe(take(1));
	}
}
