import {TestBed} from "@angular/core/testing";

import {MatPaginatorInternationalizationService} from "./mat-paginator-internationalization.service";

describe("MatPaginatorInternationalizationService", () => {
	let service: MatPaginatorInternationalizationService;

	beforeEach(() => {
		TestBed.configureTestingModule({});
		service = TestBed.inject(MatPaginatorInternationalizationService);
	});

	it("should be created", () => {
		expect(service).toBeTruthy();
	});
});
