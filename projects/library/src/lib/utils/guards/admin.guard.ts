import {inject} from "@angular/core";
import {CanActivateFn, Router} from "@angular/router";
import {FacadeService} from "@casavina/utils/facade-store/facade.service";
import {map} from "rxjs";

export const adminGuard: CanActivateFn = (route, state) => {
	const router = inject(Router);
	const facadeService = inject(FacadeService);
	return facadeService
		.isAdmin()
		.pipe(map((isAdmin) => (isAdmin ? true : router.parseUrl("/"))));
};
