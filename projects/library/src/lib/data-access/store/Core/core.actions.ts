import {createActionGroup, emptyProps} from "@ngrx/store";

export const CorePageActions = createActionGroup({
	source: "Core Page",
	events: {
		"Load Core": emptyProps()
	}
});
