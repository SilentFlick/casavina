export interface Ingredient {
	id: number;
	name: string;
}

export type CreateIngredient = Omit<Ingredient, "id">
