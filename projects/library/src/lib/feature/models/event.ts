export interface Event {
	id: string;
	title: string;
	message: string;
	createdAt: string | Date;
	expiredAt: string | Date;
}

export type CreateEvent = Omit<Event, "id" | "createAt">
