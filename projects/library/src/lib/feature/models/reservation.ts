export interface Reservation {
	id: string;
	firstName: string;
	lastName: string;
	phone: string;
	numberOfPeople: number;
	reserveAt: string;
	notice: string;
	status: string;
}

export type CreateReservation = Omit<Reservation, "id" | "status">
