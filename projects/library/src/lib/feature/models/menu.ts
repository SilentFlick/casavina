import {Allergen} from "@casavina/feature/models/allergen";
import {Ingredient} from "@casavina/feature/models/ingredient";

export interface Menu {
	id: number;
	name: string;
	category: number;
	ingredients: number[];
	description: string | null;
	allergens: number[];
	price: number[];
	image: string | null;
}

export type CreateMenu = Omit<Menu, "id">

export type MenuDetails = Omit<Menu, "ingredients" | "allergens"> & {
	ingredients: (Ingredient | undefined)[]
	allergens: (Allergen | undefined)[]
}
