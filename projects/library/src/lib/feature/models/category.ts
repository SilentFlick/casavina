export interface Category {
	id: number;
	name: string;
	menus: number[];
}

export type CreateCategory = Omit<Category, "id" | "menus">
