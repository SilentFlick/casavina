export interface Users {
	username: string;
	enabled: boolean;
}

export type CreateUser = Users & {
	password: string
}

export interface ChangePassword {
	username: string;
	oldPassword: string;
	newPassword: string;
}
