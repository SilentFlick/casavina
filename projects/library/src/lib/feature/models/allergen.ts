export interface Allergen {
	id: number;
	name: string;
	shortName: string;
}

export type CreateAllergen = Omit<Allergen, "id">
