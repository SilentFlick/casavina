export interface Offer {
	id: string;
	title: string;
	message: string;
	price: number[];
	createdAt: string | Date;
	expiredAt: string | Date;
}

export type CreateOffer = Omit<Offer, "id" | "createAt">
