import {ChangeDetectionStrategy, Component} from "@angular/core";
import {MatCardModule} from "@angular/material/card";

@Component({
	standalone: true,
	selector: "casavina-page-not-found",
	templateUrl: "page-not-found.component.html",
	styleUrls: ["page-not-found.component.scss"],
	imports: [MatCardModule],
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class PageNotFoundComponent {
}
