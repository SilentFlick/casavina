import {Directive} from "@angular/core";
import {FormGroup} from "@angular/forms";
import {FormGroupFields} from "@casavina/ui/dynamic-components/form/models/input-field";
import {InputModel} from "@casavina/ui/dynamic-components/form/models/input-model";

export interface FormActionContext {
	$implicit: FormGroup<FormGroupFields<InputModel>>;
}

@Directive({
	selector: "[csFormActions]",
	standalone: true
})
export class FormActionsDirective {
	static ngTemplateContextGuard<T>(
		dir: FormActionsDirective,
		ctx: any
	): ctx is FormActionContext {
		return true;
	}
}
