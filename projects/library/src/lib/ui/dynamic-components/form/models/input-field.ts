import {FormControl} from "@angular/forms";
import {BaseInputCommon, InputModel, InputType} from "@casavina/ui/dynamic-components/form/models/input-model";

export type InputField = Omit<BaseInputCommon, "value" | "validators"> &
	InputType & { fieldName: string }

export type FormGroupFields<T extends InputModel> = {
	[K in keyof T]: FormControl
}
