import {AsyncValidatorFn, ValidatorFn} from "@angular/forms";

export interface InputModel {
	[key: string]: BaseInputCommon & InputType;
}

export interface BaseInputCommon {
	value: string | number | boolean | Date | [] | object | null;
	label: string;
	placeholder?: string;
	autocomplete?: string;
	validators: (ValidatorFn | AsyncValidatorFn)[];
	updateOn?: "change" | "blur" | "submit";
}

export type InputType =
	| BaseInput
	| SelectInput
	| RadioInput
	| PasswordInput
	| ToggleInput
	| DateInput
	| NumberInput

interface BaseInput {
	type: "text" | "textarea" | "chips";
}

interface PasswordInput {
	type: "password";
	feedback?: boolean;
}

interface ToggleInput {
	type: "toggle";
	onLabel?: string;
	offLabel?: string;
}

interface DateInput {
	type: "date";
	time?: boolean;
	minDate?: Date;
	maxDate?: Date;
	disabledDates?: Date[];
	disabledDays?: number[];
	inline?: boolean;
}

interface SelectInput {
	type: "select";
	options: any[];
	optionLabelName: string;
	optionValueName?: string;
	multiple?: boolean;
}

interface RadioInput {
	type: "radio";
	options: {
		label: string
		value: string | number | boolean | Date | object
	}[];
}

interface NumberInput {
	type: "number";
	inputId: string;
	mode: "decimal" | "currency";
	locale?: string;
	prefix?: string;
	suffix?: string;
}
