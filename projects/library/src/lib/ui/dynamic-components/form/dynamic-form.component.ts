import {NgForOf, NgIf, NgTemplateOutlet} from "@angular/common";
import {
	ChangeDetectionStrategy,
	Component,
	ContentChild,
	EventEmitter,
	Input,
	OnInit,
	Output,
	TemplateRef
} from "@angular/core";
import {AsyncValidatorFn, FormControl, FormGroup, ReactiveFormsModule, ValidatorFn} from "@angular/forms";
import {FormGroupFields, InputField} from "@casavina/ui/dynamic-components/form/models/input-field";
import {InputModel} from "@casavina/ui/dynamic-components/form/models/input-model";
import {FormActionsDirective} from "@casavina/ui/dynamic-components/form/utils/directives/form-actions.directive";
import {CalendarModule} from "primeng/calendar";
import {ChipsModule} from "primeng/chips";
import {DropdownModule} from "primeng/dropdown";
import {InputNumberModule} from "primeng/inputnumber";
import {InputTextModule} from "primeng/inputtext";
import {InputTextareaModule} from "primeng/inputtextarea";
import {MultiSelectModule} from "primeng/multiselect";
import {PasswordModule} from "primeng/password";
import {RadioButtonModule} from "primeng/radiobutton";
import {ToggleButtonModule} from "primeng/togglebutton";

@Component({
	selector: "casavina-dynamic-form",
	standalone: true,
	imports: [
		NgForOf,
		NgIf,
		ReactiveFormsModule,
		InputTextModule,
		PasswordModule,
		ToggleButtonModule,
		RadioButtonModule,
		MultiSelectModule,
		CalendarModule,
		NgTemplateOutlet,
		InputTextareaModule,
		ChipsModule,
		DropdownModule,
		InputNumberModule
	],
	templateUrl: "dynamic-form.component.html",
	styleUrls: ["dynamic-form.component.scss"],
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class DynamicFormComponent implements OnInit {
	@Input({required: true}) public inputModel: InputModel = {};
	@Input() public groupValidators: (ValidatorFn | AsyncValidatorFn)[] = [];
	@ContentChild(FormActionsDirective, {read: TemplateRef})
	public formActionsTemplate?: TemplateRef<any>;
	@Output() outputForm: EventEmitter<FormGroup<FormGroupFields<InputModel>>> =
		new EventEmitter<FormGroup<FormGroupFields<InputModel>>>();
	// clean, without writing a lot of ngIf for each optional property of DateInput, I use this emptyObject as fallback.
	public emptyObject: any;
	protected inputField: InputField[] = [];
	// The minDate and maxDate only accept type Date in primeng version 16.5.1. Therefore, in order to keep the template
	protected dynamicForm: FormGroup<FormGroupFields<InputModel>> = new FormGroup(
		{}
	);

	ngOnInit() {
		this.dynamicForm = new FormGroup(
			this.buildFormGroupFromInputModel(this.inputModel),
			this.groupValidators
		);
		this.outputForm.emit(this.dynamicForm);
	}

	private buildFormGroupFromInputModel(inputModel: InputModel) {
		const formGroupFields: FormGroupFields<InputModel> = {};
		for (const field of Object.keys(inputModel)) {
			const fieldProperties = inputModel[field];
			formGroupFields[field] = new FormControl(fieldProperties.value, {
				validators: fieldProperties.validators,
				updateOn: fieldProperties.updateOn || "change"
			});
			switch (fieldProperties.type) {
				case "text":
				case "textarea":
				case "chips":
					this.inputField.push({
						type: fieldProperties.type,
						label: fieldProperties.label,
						fieldName: field,
						autocomplete: fieldProperties.autocomplete,
						placeholder: fieldProperties.placeholder || fieldProperties.label
					});
					break;
				case "number":
					this.inputField.push({
						type: fieldProperties.type,
						label: fieldProperties.label,
						fieldName: field,
						autocomplete: fieldProperties.autocomplete,
						placeholder: fieldProperties.placeholder || fieldProperties.label,
						inputId: fieldProperties.inputId,
						mode: fieldProperties.mode,
						locale: fieldProperties.locale,
						prefix: fieldProperties.prefix,
						suffix: fieldProperties.suffix
					});
					break;
				case "date":
					this.inputField.push({
						type: fieldProperties.type,
						label: fieldProperties.label,
						fieldName: field,
						autocomplete: fieldProperties.autocomplete,
						placeholder: fieldProperties.placeholder || fieldProperties.label,
						time: fieldProperties.time,
						minDate: fieldProperties.minDate,
						maxDate: fieldProperties.maxDate,
						disabledDates: fieldProperties.disabledDates,
						disabledDays: fieldProperties.disabledDays,
						inline: fieldProperties.inline
					});
					break;
				case "password":
					this.inputField.push({
						type: fieldProperties.type,
						label: fieldProperties.label,
						fieldName: field,
						autocomplete: fieldProperties.autocomplete,
						feedback: fieldProperties.feedback,
						placeholder: fieldProperties.placeholder || fieldProperties.label
					});
					break;
				case "toggle":
					this.inputField.push({
						type: fieldProperties.type,
						label: fieldProperties.label,
						fieldName: field,
						onLabel: fieldProperties.onLabel,
						offLabel: fieldProperties.offLabel,
						placeholder: fieldProperties.placeholder || fieldProperties.label
					});
					break;
				case "select":
					this.inputField.push({
						type: fieldProperties.type,
						label: fieldProperties.label,
						fieldName: field,
						options: fieldProperties.options,
						optionLabelName: fieldProperties.optionLabelName,
						optionValueName: fieldProperties.optionValueName,
						multiple: fieldProperties.multiple,
						placeholder: fieldProperties.placeholder || fieldProperties.label
					});
					break;
				case "radio":
					this.inputField.push({
						type: fieldProperties.type,
						label: fieldProperties.label,
						fieldName: field,
						options: fieldProperties.options,
						placeholder: fieldProperties.placeholder || fieldProperties.label
					});
					break;
			}
		}
		return formGroupFields;
	}
}
