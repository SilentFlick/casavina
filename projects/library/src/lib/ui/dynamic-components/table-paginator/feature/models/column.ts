export interface Column<TItem extends object> {
	columnDef: string;
	header: string;
	searchable?: boolean;
	filter?: {
		objectPropertyType: "text" | "numeric" | "date"
		objectPropertyName: string
	};
	cell: (element: TItem) => string | number | boolean | Date;
}
