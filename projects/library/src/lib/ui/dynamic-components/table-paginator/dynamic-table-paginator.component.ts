import {AsyncPipe, NgForOf, NgIf, NgTemplateOutlet, SlicePipe} from "@angular/common";
import {
	ChangeDetectionStrategy,
	Component,
	ContentChild,
	EventEmitter,
	Input,
	OnInit,
	Output,
	TemplateRef
} from "@angular/core";
import {FormsModule} from "@angular/forms";
import {MatPaginatorIntl, MatPaginatorModule} from "@angular/material/paginator";
import {Column} from "@casavina/ui/dynamic-components/table-paginator/feature/models/column";
import {
	TableActionsDirective
} from "@casavina/ui/dynamic-components/table-paginator/utils/directive/table-actions.directive";
import {
	TableTemplateDirective
} from "@casavina/ui/dynamic-components/table-paginator/utils/directive/table-template.directive";
import {
	MatPaginatorInternationalizationService
} from "@casavina/utils/services/mat-paginator-internationalization.service";
import {InputTextModule} from "primeng/inputtext";
import {TableModule} from "primeng/table";
import {forkJoin, iif, Observable, of, switchMap} from "rxjs";

@Component({
	selector: "casavina-dynamic-table-paginator",
	standalone: true,
	imports: [
		NgForOf,
		NgTemplateOutlet,
		TableModule,
		NgIf,
		InputTextModule,
		AsyncPipe,
		FormsModule,
		MatPaginatorModule,
		SlicePipe
	],
	providers: [
		{
			provide: MatPaginatorIntl,
			useClass: MatPaginatorInternationalizationService
		}
	],
	templateUrl: "dynamic-table-paginator.component.html",
	styleUrls: ["dynamic-table-paginator.component.scss"],
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class DynamicTablePaginatorComponent<TItem extends object>
	implements OnInit {
	@Input({required: true}) public dataSource: TItem[] = [];
	@Input({required: true}) public columns: Column<TItem>[] = [];
	@Input() public dataKey: string = "";
	@Input() public notFoundMessage: string = "";
	@ContentChild(TableTemplateDirective, {read: TemplateRef})
	public tableCellTemplate?: TemplateRef<TItem>;
	@ContentChild(TableActionsDirective, {read: TemplateRef})
	public tableActionsTemplate?: TemplateRef<TItem>;
	@Output() public selection: EventEmitter<TItem> = new EventEmitter<TItem>();
	protected defaultGlobalFilterFields: string[] = [];
	protected searchValue: string = "";
	protected selectedItem?: TItem;

	constructor() {
	}

	ngOnInit() {
		this.defaultGlobalFilterFields = this.columns
			.filter((c) => c.searchable)
			.map((c) => c.columnDef);
	}

	protected getCurrentSelectedItem(): Observable<TItem | undefined> {
		return forkJoin([of(this.selectedItem), of(this.dataSource)]).pipe(
			switchMap(([selectedItem, dataSource]) =>
				iif(
					() => selectedItem !== undefined,
					of(dataSource.find((item) => item === selectedItem)),
					of(undefined)
				)
			)
		);
	}

	protected onSelectionChange(event: TItem) {
		this.selection.emit(event);
	}
}
