import {Directive, Input, TemplateRef, ViewContainerRef} from "@angular/core";
import {Column} from "@casavina/ui/dynamic-components/table-paginator/feature/models/column";

interface TableTemplateContext<TItem extends object> {
	$implicit: TItem;
	row: TItem;
	column: Column<TItem>;
	index: number;
}

@Directive({
	selector: "[csTableTemplate]",
	standalone: true
})
export class TableTemplateDirective<TItem extends object> {
	constructor(
		private templateRef: TemplateRef<TableTemplateContext<TItem>>,
		private viewContainerRef: ViewContainerRef
	) {
	}

	@Input()
	set csTableTemplate(data: TItem[]) {
		if (data.length > 0) {
			this.viewContainerRef.createEmbeddedView(this.templateRef);
		} else {
			this.viewContainerRef.clear();
		}
	}

	static ngTemplateContextGuard<TContextItem extends object>(
		dir: TableTemplateDirective<TContextItem>,
		ctx: unknown
	): ctx is TableTemplateContext<TContextItem> {
		return true;
	}
}
