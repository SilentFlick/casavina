import {Directive} from "@angular/core";

@Directive({
	selector: "[csTableActions]",
	standalone: true
})
export class TableActionsDirective {
	constructor() {
	}
}
