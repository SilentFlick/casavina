export interface ListInputModel<TItem extends object> {
	title: (item: TItem) => string | number | boolean | Date;
	itemProperties: {
		label: string
		icon?: string
		value: (item: TItem) => string | number | boolean | Date
	}[];
}
