import {ComponentFixture, TestBed} from "@angular/core/testing";

import {ListPaginatorComponent} from "./list-paginator.component";

describe("ListPaginatorComponent", () => {
	let component: ListPaginatorComponent<object>;
	let fixture: ComponentFixture<ListPaginatorComponent<object>>;

	beforeEach(() => {
		TestBed.configureTestingModule({
			imports: [ListPaginatorComponent]
		});
		fixture = TestBed.createComponent(ListPaginatorComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it("should create", () => {
		expect(component).toBeTruthy();
	});
});
