import {NgForOf, NgIf, NgTemplateOutlet, SlicePipe} from "@angular/common";
import {ChangeDetectionStrategy, Component, EventEmitter, Input, Output} from "@angular/core";
import {MatButtonModule} from "@angular/material/button";
import {MatExpansionModule} from "@angular/material/expansion";
import {MatIconModule} from "@angular/material/icon";
import {MatListModule, MatSelectionListChange} from "@angular/material/list";
import {MatPaginatorIntl, MatPaginatorModule} from "@angular/material/paginator";
import {ListInputModel} from "@casavina/ui/dynamic-components/list-paginator/feature/models/list-input-model";
import {
	MatPaginatorInternationalizationService
} from "@casavina/utils/services/mat-paginator-internationalization.service";

@Component({
	selector: "casavina-dynamic-list-paginator",
	standalone: true,
	imports: [
		MatListModule,
		MatButtonModule,
		MatIconModule,
		NgForOf,
		NgIf,
		MatPaginatorModule,
		SlicePipe,
		MatExpansionModule,
		NgTemplateOutlet
	],
	providers: [
		{
			provide: MatPaginatorIntl,
			useClass: MatPaginatorInternationalizationService
		}
	],
	templateUrl: "./list-paginator.component.html",
	styleUrls: ["./list-paginator.component.scss"],
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class ListPaginatorComponent<TItem extends object> {
	@Input({required: true}) public dataSource: TItem[] = [];
	@Input({required: true}) public listInputModel: ListInputModel<TItem> = {
		title: (_) => "",
		itemProperties: []
	};
	@Input() public multiple: boolean = false;
	@Output() public selectedOptions = new EventEmitter<TItem[]>();

	onSelectionChange(event: MatSelectionListChange): void {
		this.selectedOptions.emit(event.options.map((item) => item.value as TItem));
	}
}
