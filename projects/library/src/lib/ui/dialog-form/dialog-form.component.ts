import {NgIf} from "@angular/common";
import {ChangeDetectionStrategy, Component} from "@angular/core";
import {AsyncValidatorFn, FormGroup, ValidatorFn} from "@angular/forms";
import {MatButtonModule} from "@angular/material/button";
import {MatIconModule} from "@angular/material/icon";
import {DynamicFormComponent} from "@casavina/ui/dynamic-components/form/dynamic-form.component";
import {InputModel} from "@casavina/ui/dynamic-components/form/models/input-model";
import {FormActionsDirective} from "@casavina/ui/dynamic-components/form/utils/directives/form-actions.directive";
import {DynamicDialogConfig, DynamicDialogRef} from "primeng/dynamicdialog";

@Component({
	selector: "casavina-dialog-form",
	standalone: true,
	imports: [
		DynamicFormComponent,
		MatButtonModule,
		MatIconModule,
		FormActionsDirective,
		NgIf
	],
	templateUrl: "dialog-form.component.html",
	styleUrls: ["dynamic-form.component.scss"],
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class DialogFormComponent {
	protected data: {
		inputModel: InputModel
		groupValidators: (ValidatorFn | AsyncValidatorFn)[]
	} = {
		inputModel: {},
		groupValidators: []
	};

	constructor(
		private dynamicDialogConfig: DynamicDialogConfig,
		private dynamicDialogRef: DynamicDialogRef
	) {
		this.data = this.dynamicDialogConfig.data;
	}

	public submitForm(form: FormGroup) {
		this.dynamicDialogRef.close(form.value);
	}
}
