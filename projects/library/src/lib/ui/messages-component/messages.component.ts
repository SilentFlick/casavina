import {ChangeDetectionStrategy, Component, Input} from "@angular/core";
import {Message} from "primeng/api";
import {MessagesModule} from "primeng/messages";

@Component({
	selector: "casavina-messages",
	standalone: true,
	imports: [MessagesModule],
	template: `
		<p-messages [(value)] = "message" [enableService] = "false" [closable] = "true" ></p-messages >
	`,
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class MessagesComponent {
	@Input({required: true})
	public message!: Message[];
}
