import {ChangeDetectionStrategy, Component} from "@angular/core";
import {MatProgressBarModule} from "@angular/material/progress-bar";

@Component({
	selector: "casavina-progress-bar-indeterminate",
	standalone: true,
	imports: [MatProgressBarModule],
	template: `
		<mat-progress-bar mode = "indeterminate" color = "accent" ></mat-progress-bar >
	`,
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class ProgressBarIndeterminateComponent {
}
