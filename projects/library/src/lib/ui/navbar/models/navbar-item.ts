import {Observable} from "rxjs";

export interface NavbarItem {
	label: string;
	routerLink?: string;
	items?: Omit<NavbarItem, "items">[];
	visible?: Observable<boolean>;
}
