import {ComponentFixture, TestBed} from "@angular/core/testing";

import {NavbarItemsMobileComponent} from "./navbar-items-mobile.component";

describe("NavbarItemsMobileComponent", () => {
	let component: NavbarItemsMobileComponent;
	let fixture: ComponentFixture<NavbarItemsMobileComponent>;

	beforeEach(() => {
		TestBed.configureTestingModule({
			imports: [NavbarItemsMobileComponent]
		});
		fixture = TestBed.createComponent(NavbarItemsMobileComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it("should create", () => {
		expect(component).toBeTruthy();
	});
});
