import {Component} from "@angular/core";
import {MatButtonModule} from "@angular/material/button";
import {MatIconModule} from "@angular/material/icon";

@Component({
	standalone: true,
	selector: "casavina-flyer",
	templateUrl: "flyer.component.html",
	imports: [MatButtonModule, MatIconModule]
})
export class FlyerComponent {
}
