import {NgIf} from "@angular/common";
import {ChangeDetectionStrategy, Component, Input} from "@angular/core";
import {ProgressBarIndeterminateComponent} from "@casavina/ui/loading-components/progress-bar-indeterminate.component";
import {MessagesComponent} from "@casavina/ui/messages-component/messages.component";

@Component({
	selector: "casavina-loading-error-status",
	standalone: true,
	imports: [ProgressBarIndeterminateComponent, MessagesComponent, NgIf],
	templateUrl: "./loading-error-status.component.html",
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class LoadingErrorStatusComponent {
	@Input({required: true}) public status: string = "loading";
	@Input({required: true}) public error: string = "";
}
