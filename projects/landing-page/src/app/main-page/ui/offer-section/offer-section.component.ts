import {CurrencyPipe, NgForOf, NgIf} from "@angular/common";
import {ChangeDetectionStrategy, Component, Input} from "@angular/core";
import {MatExpansionModule} from "@angular/material/expansion";
import {Offer} from "@casavina/feature/models/offer";
import {LoadingErrorStatusComponent} from "@casavina/ui/loading-error-status/loading-error-status.component";

@Component({
	selector: "casavina-offer-section",
	standalone: true,
	imports: [
		LoadingErrorStatusComponent,
		NgIf,
		MatExpansionModule,
		CurrencyPipe,
		NgForOf
	],
	templateUrl: "./offer-section.component.html",
	styleUrls: ["./offer-section.component.scss"],
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class OfferSectionComponent {
	@Input({required: true}) public offers: Offer[] = [];
	@Input({required: true}) public status: string = "";
	@Input({required: true}) public error: string = "";
}
