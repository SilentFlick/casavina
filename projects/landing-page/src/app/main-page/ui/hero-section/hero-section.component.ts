import {ChangeDetectionStrategy, Component} from "@angular/core";
import {MatButtonModule} from "@angular/material/button";
import {RouterLink} from "@angular/router";

@Component({
	selector: "casavina-hero-section",
	standalone: true,
	imports: [MatButtonModule, RouterLink],
	templateUrl: "./hero-section.component.html",
	styleUrls: ["./hero-section.component.scss"],
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class HeroSectionComponent {
}
