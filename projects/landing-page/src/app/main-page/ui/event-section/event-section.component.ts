import {NgForOf, NgIf} from "@angular/common";
import {ChangeDetectionStrategy, Component, Input} from "@angular/core";
import {MatExpansionModule} from "@angular/material/expansion";
import {MatStepperModule} from "@angular/material/stepper";
import {Event} from "@casavina/feature/models/event";
import {LoadingErrorStatusComponent} from "@casavina/ui/loading-error-status/loading-error-status.component";

@Component({
	selector: "casavina-event-section",
	standalone: true,
	imports: [
		MatStepperModule,
		NgForOf,
		NgIf,
		LoadingErrorStatusComponent,
		MatExpansionModule
	],
	templateUrl: "./event-section.component.html",
	styleUrls: ["./event-section.component.scss"],
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class EventSectionComponent {
	@Input({required: true}) public events: Event[] = [];
	@Input({required: true}) public status: string | null = "";
	@Input({required: true}) public error: string | null = "";
}
