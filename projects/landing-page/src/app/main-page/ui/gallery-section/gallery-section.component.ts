import {ChangeDetectionStrategy, Component} from "@angular/core";
import {GalleriaModule} from "primeng/galleria";

@Component({
	selector: "casavina-gallery-section",
	standalone: true,
	imports: [GalleriaModule],
	templateUrl: "./gallery-section.component.html",
	styleUrls: ["./gallery-section.component.scss"],
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class GallerySectionComponent {
	public imageUrls = [
		"assets/2.webp",
		"assets/3.webp",
		"assets/4.webp",
		"assets/6.webp",
		"assets/7.webp",
		"assets/1.webp",
		"assets/5.webp"
	];
}
