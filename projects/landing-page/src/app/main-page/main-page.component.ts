import {AsyncPipe, NgForOf, NgIf} from "@angular/common";
import {ChangeDetectionStrategy, Component, OnInit} from "@angular/core";
import {MatButtonModule} from "@angular/material/button";
import {MatDividerModule} from "@angular/material/divider";
import {MatIconModule} from "@angular/material/icon";
import {MatStepperModule} from "@angular/material/stepper";
import {RouterLink} from "@angular/router";
import {Event} from "@casavina/feature/models/event";
import {Offer} from "@casavina/feature/models/offer";
import {LoadingErrorStatusComponent} from "@casavina/ui/loading-error-status/loading-error-status.component";
import {FacadeService} from "@casavina/utils/facade-store/facade.service";
import {Observable} from "rxjs";
import {EventSectionComponent} from "./ui/event-section/event-section.component";
import {GallerySectionComponent} from "./ui/gallery-section/gallery-section.component";
import {HeroSectionComponent} from "./ui/hero-section/hero-section.component";
import {OfferSectionComponent} from "./ui/offer-section/offer-section.component";

@Component({
	standalone: true,
	selector: "casavina-customers",
	templateUrl: "./main-page.component.html",
	styleUrls: ["./main-page.component.scss"],
	imports: [
		MatButtonModule,
		MatDividerModule,
		NgIf,
		AsyncPipe,
		NgForOf,
		MatStepperModule,
		LoadingErrorStatusComponent,
		RouterLink,
		HeroSectionComponent,
		OfferSectionComponent,
		EventSectionComponent,
		GallerySectionComponent,
		MatIconModule
	],
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class MainPageComponent implements OnInit {
	public event$!: Observable<Event[]>;
	public eventStatus$!: Observable<string>;
	public eventError$!: Observable<string>;
	public offer$!: Observable<Offer[]>;
	public offerStatus$!: Observable<string>;
	public offerError$!: Observable<string>;
	public imageUrls = [
		"assets/1.webp",
		"assets/2.webp",
		"assets/3.webp",
		"assets/4.webp",
		"assets/5.webp",
		"assets/6.webp",
		"assets/7.webp"
	];

	constructor(protected facadeService: FacadeService) {
	}

	ngOnInit(): void {
		this.facadeService.loadEventState();
		this.facadeService.loadOfferState();
		this.event$ = this.facadeService.getAllEvent();
		this.eventStatus$ = this.facadeService.getEventStatus();
		this.eventError$ = this.facadeService.getEventError();
		this.offer$ = this.facadeService.getAllOffer();
		this.offerStatus$ = this.facadeService.getOfferStatus();
		this.offerError$ = this.facadeService.getOfferError();
	}
}