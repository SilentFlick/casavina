import {NgForOf} from "@angular/common";
import {AfterViewInit, Component} from "@angular/core";
import {MatButtonModule} from "@angular/material/button";
import {MatGridListModule} from "@angular/material/grid-list";
import {MatTableModule} from "@angular/material/table";
import {icon, map, marker, tileLayer} from "leaflet";

export interface BusinessHours {
	day: string;
	hours: string[];
}

@Component({
	standalone: true,
	selector: "casavina-location",
	templateUrl: "./location.component.html",
	styleUrls: ["./location.component.scss"],
	imports: [MatGridListModule, MatButtonModule, MatTableModule, NgForOf]
})
export class LocationComponent implements AfterViewInit {
	public businessHours: BusinessHours[] = [
		{day: "Montag", hours: ["Geschlossen"]},
		{day: "Dienstag", hours: ["Geschlossen"]},
		{day: "Mittwoch", hours: ["11:30-14:00", "17:30-22:00"]},
		{day: "Donnerstag", hours: ["11:30-14:00", "17:30-22:00"]},
		{day: "Freitag", hours: ["11:30-14:00", "ab 17:30"]},
		{day: "Samstag", hours: ["11:30-14:00", "ab 17:30"]},
		{day: "Sonntag", hours: ["11:30-14:00", "17:00-21:00"]}
	];
	public displayedColumns: string[] = ["day", "hours"];
	public dataSource = this.businessHours;

	constructor() {
	}

	ngAfterViewInit() {
		const m = map("map").setView([51.0042, 13.68773], 19);

		tileLayer("https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png", {
			maxZoom: 19,
			attribution:
				"&copy; <a href=\"https://www.openstreetmap.org/copyright\">OpenStreetMap</a>"
		}).addTo(m);
		const icons = {
			icon: icon({
				iconSize: [25, 41],
				iconAnchor: [13, 0],
				// specify the path here
				iconUrl: "assets/marker-icon.webp",
				shadowUrl: "assets/marker-shadow.webp"
			})
		};
		marker([51.00429497320438, 13.687697430049896], icons)
			.addTo(m)
			.bindPopup(
				"Karlsruher Straße 143,<br>" +
				"01189 Dresden<br>" +
				"Tel. <a href=\"tel:+493514011114\">+49 351 4011114</a><br>" +
				"E-Mail <a href=\"mailto:contact@casavina.de\">contact@casavina.de</a>"
			);
	}
}
