import {AsyncPipe, CurrencyPipe, NgForOf, NgIf} from "@angular/common";
import {ChangeDetectionStrategy, Component, Input, OnInit} from "@angular/core";
import {MatButtonModule} from "@angular/material/button";
import {MatCardModule} from "@angular/material/card";
import {MatDividerModule} from "@angular/material/divider";
import {MatTableModule} from "@angular/material/table";
import {MatTabsModule} from "@angular/material/tabs";
import {Category} from "@casavina/feature/models/category";
import {FlyerComponent} from "@casavina/ui/flyer/flyer.component";
import {FacadeService} from "@casavina/utils/facade-store/facade.service";
import {Observable} from "rxjs";

@Component({
	standalone: true,
	selector: "casavina-menu-items",
	templateUrl: "./menu-items.component.html",
	styleUrls: ["./menu-items.component.scss"],
	imports: [
		MatButtonModule,
		MatTabsModule,
		NgForOf,
		NgIf,
		AsyncPipe,
		MatCardModule,
		CurrencyPipe,
		MatTableModule,
		FlyerComponent,
		MatDividerModule
	],
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class MenuItemsComponent implements OnInit {
	public menuStatus$!: Observable<string>;
	public menuError$!: Observable<string>;
	@Input() category$!: Observable<Category[]>;

	constructor(protected facadeService: FacadeService) {
	}

	ngOnInit() {
		this.menuStatus$ = this.facadeService.getMenuStatus();
		this.menuError$ = this.facadeService.getMenuError();
	}
}
