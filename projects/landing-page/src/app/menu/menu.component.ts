import {AsyncPipe, NgIf} from "@angular/common";
import {ChangeDetectionStrategy, Component, OnInit} from "@angular/core";
import {Category} from "@casavina/feature/models/category";
import {FlyerComponent} from "@casavina/ui/flyer/flyer.component";
import {ProgressBarIndeterminateComponent} from "@casavina/ui/loading-components/progress-bar-indeterminate.component";
import {LoadingErrorStatusComponent} from "@casavina/ui/loading-error-status/loading-error-status.component";
import {MessagesComponent} from "@casavina/ui/messages-component/messages.component";
import {FacadeService} from "@casavina/utils/facade-store/facade.service";
import {Observable} from "rxjs";
import {MenuItemsComponent} from "./menu-items/menu-items.component";

@Component({
	standalone: true,
	selector: "casavina-menu",
	templateUrl: "./menu.component.html",
	styleUrls: ["./menu.component.scss"],
	imports: [
		AsyncPipe,
		MenuItemsComponent,
		NgIf,
		ProgressBarIndeterminateComponent,
		MessagesComponent,
		FlyerComponent,
		LoadingErrorStatusComponent
	],
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class MenuComponent implements OnInit {
	public allCategories$!: Observable<Category[]>;
	public categoryStatus$!: Observable<string>;
	public categoryError$!: Observable<string>;

	constructor(private facadeService: FacadeService) {
	}

	ngOnInit(): void {
		this.facadeService.loadCoreState();
		this.allCategories$ = this.facadeService.getAllCategory();
		this.categoryStatus$ = this.facadeService.getCategoryStatus();
		this.categoryError$ = this.facadeService.getCategoryError();
	}
}
