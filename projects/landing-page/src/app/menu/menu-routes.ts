import {Route} from "@angular/router";

export default [
	{
		path: "",
		title: "Casa Vina - Menu",
		loadComponent: () =>
			import("./menu.component").then((c) => c.MenuComponent)
	}
] as Route[];
