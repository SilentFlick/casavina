import {Injectable} from "@angular/core";
import {AbstractControl, ValidationErrors, Validators} from "@angular/forms";
import {InputModel} from "@casavina/ui/dynamic-components/form/models/input-model";

@Injectable()
export class ReservationInputModelService {
	public getInputModel(): InputModel {
		return {
			firstName: {
				type: "text",
				value: "",
				label: "Vorname",
				validators: [Validators.required, Validators.maxLength(255)]
			},
			lastName: {
				type: "text",
				value: "",
				label: "Nachname",
				validators: [Validators.required, Validators.maxLength(255)]
			},
			phone: {
				type: "text",
				value: "",
				label: "Telefonnummer",
				validators: [
					Validators.required,
					Validators.maxLength(20),
					Validators.minLength(10),
					Validators.pattern("[- +()0-9]{11,}")
				]
			},
			numberOfPeople: {
				type: "number",
				value: null,
				label: "Gruppengröße",
				validators: [
					Validators.required,
					Validators.max(200),
					Validators.min(1)
				],
				mode: "decimal",
				inputId: "integeronly",
				locale: "de"
			},
			reserveAt: {
				type: "date",
				value: this.getMinDate(),
				label: "Datum und Uhrzeit",
				validators: [this.reserveAtValidation],
				time: true,
				disabledDays: [1, 2],
				disabledDates: [new Date("2024-12-25"), new Date("2024-12-26")],
				inline: true,
				minDate: this.getMinDate(),
				maxDate: this.getMaxDate()
			},
			notice: {
				type: "textarea",
				value: "",
				label: "Notiz",
				validators: [Validators.maxLength(255)]
			}
		};
	}

	private reserveAtValidation(
		control: AbstractControl
	): ValidationErrors | null {
		const chosenDayOfWeek = control.value.getDay();
		const chosenDate: Date = new Date(control.value);
		const openingHoursFromChosenDate: {
			[dayOfWeek: number]: [number, number][]
		} = {
			0: [
				[chosenDate.setHours(11, 30), chosenDate.setHours(14, 0)],
				[chosenDate.setHours(17, 0), chosenDate.setHours(21, 0)]
			],
			3: [
				[chosenDate.setHours(11, 30), chosenDate.setHours(14, 0)],
				[chosenDate.setHours(17, 30), chosenDate.setHours(22, 0)]
			],
			4: [
				[chosenDate.setHours(11, 30), chosenDate.setHours(14, 0)],
				[chosenDate.setHours(17, 30), chosenDate.setHours(22, 0)]
			],
			5: [
				[chosenDate.setHours(11, 30), chosenDate.setHours(14, 0)],
				[chosenDate.setHours(17, 0), chosenDate.setHours(23, 59)]
			],
			6: [
				[chosenDate.setHours(11, 30), chosenDate.setHours(14, 0)],
				[chosenDate.setHours(17, 0), chosenDate.setHours(23, 59)]
			]
		};
		for (const [startTime, endTime] of openingHoursFromChosenDate[
			chosenDayOfWeek
			]) {
			if (
				control.value.getTime() >= startTime &&
				control.value.getTime() <= endTime
			) {
				return null;
			}
		}
		return {invalidReserveAt: true};
	}

	private getMinDate(): Date {
		let date: Date = new Date();
		date.setDate(date.getDate() + 3);
		while (date.getDay() === 1 || date.getDay() === 2) {
			date.setDate(date.getDate() + 1);
		}
		date.setHours(11, 30, 0);
		return date;
	}

	private getMaxDate(): Date {
		const date: Date = new Date();
		return new Date(
			date.setFullYear(date.getFullYear() + 3, date.getMonth(), date.getDate())
		);
	}
}