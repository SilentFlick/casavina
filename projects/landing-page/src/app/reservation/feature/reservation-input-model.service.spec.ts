import {TestBed} from "@angular/core/testing";

import {ReservationInputModelService} from "./reservation-input-model.service";

describe("ReservationInputModelService", () => {
	let service: ReservationInputModelService;

	beforeEach(() => {
		TestBed.configureTestingModule({});
		service = TestBed.inject(ReservationInputModelService);
	});

	it("should be created", () => {
		expect(service).toBeTruthy();
	});
});
