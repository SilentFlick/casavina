import {DatePipe} from "@angular/common";
import {ChangeDetectionStrategy, Component, OnInit} from "@angular/core";
import {FormGroup} from "@angular/forms";
import {MatButtonModule} from "@angular/material/button";
import {MatIconModule} from "@angular/material/icon";
import {CreateReservation} from "@casavina/feature/models/reservation";
import {DynamicFormComponent} from "@casavina/ui/dynamic-components/form/dynamic-form.component";
import {FormGroupFields} from "@casavina/ui/dynamic-components/form/models/input-field";
import {InputModel} from "@casavina/ui/dynamic-components/form/models/input-model";
import {FormActionsDirective} from "@casavina/ui/dynamic-components/form/utils/directives/form-actions.directive";
import {LoadingErrorStatusComponent} from "@casavina/ui/loading-error-status/loading-error-status.component";
import {FacadeService} from "@casavina/utils/facade-store/facade.service";
import {ConfirmationService} from "primeng/api";
import {ReservationInputModelService} from "../../feature/reservation-input-model.service";

@Component({
	selector: "casavina-reservation-form",
	standalone: true,
	imports: [
		DynamicFormComponent,
		FormActionsDirective,
		MatButtonModule,
		MatIconModule,
		LoadingErrorStatusComponent
	],
	providers: [DatePipe],
	templateUrl: "./reservation-form.component.html",
	styleUrls: ["./reservation-form.component.scss"],
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class ReservationFormComponent implements OnInit {
	public inputModel: InputModel = {};

	constructor(
		private reservationInputModelService: ReservationInputModelService,
		private datePipe: DatePipe,
		private confirmationService: ConfirmationService,
		private facadeService: FacadeService
	) {
	}

	ngOnInit() {
		this.inputModel = this.reservationInputModelService.getInputModel();
	}

	public createReservation(form: FormGroup<FormGroupFields<InputModel>>) {
		const reserveAt = form.get("reserveAt")!.value;
		const formattedDate =
			this.datePipe.transform(reserveAt, "dd.MM.YYYY 'um' HH:mm 'Uhr'", "de") ||
			"";
		const iso8601ReserveAt =
			this.datePipe.transform(reserveAt, "YYYY-MM-ddTHH:mm", "de") || "";
		this.confirmationService.confirm({
			header: "Bestätigen Sie Ihre Reservierung",
			message: `Sind Sie sicher, dass Sie einen Tisch für
          ${form.get("numberOfPeople")!.value} Person(en) am
          ${formattedDate} reservieren
          möchten?`,
			accept: () => {
				const reservation: CreateReservation = {
					firstName: form.get("firstName")?.value,
					lastName: form.get("lastName")?.value,
					notice: form.get("notice")?.value,
					numberOfPeople: form.get("numberOfPeople")?.value,
					phone: form.get("phone")?.value.replace(/\s+/g, ""),
					reserveAt: iso8601ReserveAt
				};
				this.facadeService.createReservation(reservation);
			}
		});
	}
}
