import {DatePipe, NgIf} from "@angular/common";
import {ChangeDetectionStrategy, Component} from "@angular/core";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {MatButtonModule} from "@angular/material/button";
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatInputModule} from "@angular/material/input";
import {MatSnackBarModule} from "@angular/material/snack-bar";
import { Message } from "primeng/api";
import {CalendarModule} from "primeng/calendar";
import { MessagesModule } from "primeng/messages";
import {ReservationInputModelService} from "./feature/reservation-input-model.service";
import {ReservationFormComponent} from "./ui/reservation-form/reservation-form.component";

@Component({
	providers: [DatePipe, ReservationInputModelService],
	imports: [
		MatFormFieldModule,
		ReactiveFormsModule,
		MatInputModule,
		FormsModule,
		MatButtonModule,
		MatSnackBarModule,
		CalendarModule,
		DatePipe,
		ReservationFormComponent,
		MessagesModule,
		NgIf
	],
	selector: "casavina-reservation",
	standalone: true,
	styleUrls: ["./reservation.component.scss"],
	templateUrl: "./reservation.component.html",
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class ReservationComponent {
	message: Message[] = [{
		severity: "info",
		summary: "",
		detail: "Reservierungen für den 25. und 26.12. sind nur telefonisch möglich."
	}]

	temporary = new Date() <= new Date("2024-12-26")
}