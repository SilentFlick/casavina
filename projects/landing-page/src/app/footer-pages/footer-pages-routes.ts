import {Route} from "@angular/router";

export default [
	{
		path: "impressum",
		title: "Casa Vina - Impressum",
		loadComponent: () =>
			import("./imprint/imprint.component").then((c) => c.ImprintComponent)
	},
	{
		path: "datenschutz",
		title: "Casa Vina - Datenschutz",
		loadComponent: () =>
			import("./privacy/privacy.component").then((c) => c.PrivacyComponent)
	},
	{
		path: "einwilligungsverwaltung",
		title: "Casa Vina - Einwilligungsverwaltung",
		loadComponent: () =>
			import("./consent/consent.component").then((c) => c.ConsentComponent)
	},
	{
		path: "agb",
		title: "Casa Vina - AGB",
		loadComponent: () =>
			import("./agb/agb.component").then((c) => c.AgbComponent)
	}
] as Route[];
