import {ChangeDetectionStrategy, Component} from "@angular/core";

@Component({
	selector: "casavina-consent",
	standalone: true,
	imports: [],
	templateUrl: "./consent.component.html",
	styleUrls: ["./consent.component.scss"],
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class ConsentComponent {
}
