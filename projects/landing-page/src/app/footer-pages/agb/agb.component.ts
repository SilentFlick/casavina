import {ChangeDetectionStrategy, Component} from "@angular/core";

@Component({
	selector: "casavina-agb",
	standalone: true,
	imports: [],
	templateUrl: "./agb.component.html",
	styleUrls: ["./agb.component.scss"],
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class AgbComponent {
}
