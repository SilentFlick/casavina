import {ChangeDetectionStrategy, Component} from "@angular/core";

@Component({
	selector: "casavina-imprint",
	standalone: true,
	imports: [],
	templateUrl: "./imprint.component.html",
	styleUrls: ["./imprint.component.scss"],
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class ImprintComponent {
}
