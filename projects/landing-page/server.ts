import "zone.js/node";

import {APP_BASE_HREF} from "@angular/common";
import {ngExpressEngine} from "@nguniversal/express-engine";
import express from "express";
import {existsSync} from "node:fs";
import {join, resolve} from "node:path";
import bootstrap from "./src/main.server";

export function app(): express.Express {
	const server = express();
	const serverDistFolder = __dirname;
	const browserDistFolder = resolve(serverDistFolder, "../browser");
	const indexHtml = existsSync(join(browserDistFolder, "index.original.html"))
		? "index.original.html"
		: "index";

	server.engine(
		"html",
		ngExpressEngine({
			bootstrap
		})
	);

	server.set("view engine", "html");
	server.set("views", browserDistFolder);

	server.use((req, res, next) => {
		res.setHeader(
			"Content-Security-Policy",
			[
				"default-src 'none'",
				"base-uri 'self'",
				"child-src 'self'",
				"connect-src 'self' api.casavina.de",
				"font-src 'self'",
				"form-action 'self' api.casavina.de",
				"frame-ancestors 'none'",
				"frame-src 'none'",
				"img-src 'self' api.casavina.de *.tile.openstreetmap.org",
				"manifest-src 'self'",
				"media-src 'self' api.casavina.de",
				"script-src 'self' 'unsafe-inline' 'unsafe-eval'",
				"style-src 'self' 'unsafe-inline'",
				"upgrade-insecure-requests",
				"worker-src 'none'"
			].join("; ")
		);
		next();
	});

	server.get(
		"*.*",
		express.static(browserDistFolder, {
			maxAge: "1y"
		})
	);

	server.get("*", (req, res) => {
		res.render(indexHtml, {
			req,
			providers: [{provide: APP_BASE_HREF, useValue: req.baseUrl}]
		});
	});

	return server;
}

function run(): void {
	const port = process.env["PORT"] || 4000;

	const server = app();
	server.listen(port, () => {
		console.log(`Node Express server listening on http://localhost:${port}`);
	});
}

declare const __non_webpack_require__: NodeRequire;
const mainModule = __non_webpack_require__.main;
const moduleFilename = (mainModule && mainModule.filename) || "";
if (moduleFilename === __filename || moduleFilename.includes("iisnode")) {
	run();
}

export default bootstrap;
