importScripts('./ngsw-worker.js')

let url
let intervalId
let accessToken

self.addEventListener('message', (event) => {
    if (event.data && event.data.url && event.data.accessToken) {
        url = event.data.url
        accessToken = event.data.accessToken
        intervalId = setInterval(renewToken, 25 * 60 * 1000)
    } else if (event.data === 'stopRenewInterval') {
        clearInterval(intervalId)
        accessToken = ''
    }
})

function renewToken() {
    const renewUrl = new URL('/v1/auth/renew', url)
    const headers = new Headers()
    headers.append('Content-Type', 'application/json')
    headers.append('Authorization', 'Bearer ' + accessToken)
    fetch(renewUrl, {
        method: 'POST',
        headers: headers,
        credentials: 'include',
        body: null,
    })
        .then((response) => {
            if (!response.ok) {
                throw new Error('HTTP error, status = ' + response.status)
            }
            return response.json()
        })
        .then((data) => {
            accessToken = data.accessToken
        })
        .catch((error) => console.error('POST request failed:', error))
}
