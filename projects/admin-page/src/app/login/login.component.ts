import {AsyncPipe, NgIf} from "@angular/common";
import {ChangeDetectionStrategy, Component, OnInit} from "@angular/core";
import {FormGroup, Validators} from "@angular/forms";
import {MatButtonModule} from "@angular/material/button";
import {MatIconModule} from "@angular/material/icon";
import {DynamicFormComponent} from "@casavina/ui/dynamic-components/form/dynamic-form.component";
import {FormGroupFields} from "@casavina/ui/dynamic-components/form/models/input-field";
import {InputModel} from "@casavina/ui/dynamic-components/form/models/input-model";
import {FormActionsDirective} from "@casavina/ui/dynamic-components/form/utils/directives/form-actions.directive";
import {LoadingErrorStatusComponent} from "@casavina/ui/loading-error-status/loading-error-status.component";
import {FacadeService} from "@casavina/utils/facade-store/facade.service";
import {Observable} from "rxjs";

@Component({
	standalone: true,
	selector: "casavina-login",
	templateUrl: "./login.component.html",
	styleUrls: ["./login.component.scss"],
	imports: [
		AsyncPipe,
		NgIf,
		DynamicFormComponent,
		LoadingErrorStatusComponent,
		FormActionsDirective,
		MatButtonModule,
		MatIconModule
	],
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class LoginComponent implements OnInit {
	public loginForm: FormGroup = new FormGroup({});
	public inputModel: InputModel = {
		username: {
			type: "text",
			value: "",
			label: "Benutzername",
			autocomplete: "username",
			validators: [Validators.required]
		},
		password: {
			type: "password",
			value: "",
			label: "Passwort",
			autocomplete: "current-password",
			validators: [Validators.required]
		}
	};
	public authStatus$!: Observable<string>;
	public authError$!: Observable<string>;

	constructor(private facadeService: FacadeService) {
	}

	ngOnInit() {
		this.authStatus$ = this.facadeService.getAuthStatus();
		this.authError$ = this.facadeService.getAuthError();
	}

	onSubmit(form: FormGroup<FormGroupFields<InputModel>>): void {
		this.facadeService.login(
			form.controls.username.value,
			form.controls.password.value
		);
	}
}
