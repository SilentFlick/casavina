import {Injectable} from "@angular/core";
import {InputModel} from "@casavina/ui/dynamic-components/form/models/input-model";
import {FacadeService} from "@casavina/utils/facade-store/facade.service";
import {DialogsService} from "@casavina/utils/services/dialogs.service";
import {ConfirmationService} from "primeng/api";
import {EventOfferService} from "../../feature/event-offer.service";
import {EventModelService} from "../feature/event-model.service";

@Injectable()
export class EventService extends EventOfferService {
	constructor(
		private facadeService: FacadeService,
		private dialogService: DialogsService,
		private confirmationService: ConfirmationService,
		private eventModelService: EventModelService
	) {
		super();
	}

	public createEntity() {
		this.dialogService
			.openFormDialog(
				this.eventModelService.getInputModel(),
				"Veranstaltung Hinzufügen"
			)
			.subscribe((data) => {
				if (data) {
					this.facadeService.createEvent(data);
				}
			});
	}

	public deleteEntityById(id: string, title: string) {
		this.confirmationService.confirm({
			header: "Löschung bestätigen",
			message: `Sind Sie sicher, dass Sie die Veranstaltung '${title}' löschen möchten?`,
			accept: () => {
				this.facadeService.deleteEvent(id);
			},
			reject: () => {
			},
			acceptButtonStyleClass: "p-button-danger"
		});
	}

	public editEntity(id: string, inputModel: InputModel) {
		this.dialogService
			.openFormDialog(inputModel, "Veranstaltung Ändern")
			.subscribe((data) => {
				if (data) {
					this.facadeService.updateEvent({
						...data,
						id: id
					});
				}
			});
	}
}
