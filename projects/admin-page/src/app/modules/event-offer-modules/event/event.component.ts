import {BreakpointObserver, Breakpoints, LayoutModule} from "@angular/cdk/layout";
import {AsyncPipe, NgIf} from "@angular/common";
import {ChangeDetectionStrategy, Component, OnInit} from "@angular/core";
import {MatButtonModule} from "@angular/material/button";
import {MatIconModule} from "@angular/material/icon";
import {Event} from "@casavina/feature/models/event";
import {LoadingErrorStatusComponent} from "@casavina/ui/loading-error-status/loading-error-status.component";
import {FacadeService} from "@casavina/utils/facade-store/facade.service";
import {map, Observable} from "rxjs";
import {EventOfferService} from "../feature/event-offer.service";
import {MetadataModelService} from "../feature/metadata-model.service";
import {EventOfferListComponent} from "../ui/event-offer-list/event-offer-list.component";
import {EventOfferTableComponent} from "../ui/event-offer-table/event-offer-table.component";
import {EventModelService} from "./feature/event-model.service";
import {EventService} from "./utils/event.service";

@Component({
	selector: "casavina-event",
	standalone: true,
	imports: [
		NgIf,
		AsyncPipe,
		LayoutModule,
		LoadingErrorStatusComponent,
		EventOfferListComponent,
		EventOfferTableComponent,
		MatButtonModule,
		MatIconModule
	],
	providers: [
		EventService,
		EventModelService,
		{
			provide: EventOfferService,
			useClass: EventService
		},
		{
			provide: MetadataModelService,
			useClass: EventModelService
		}
	],
	templateUrl: "event.component.html",
	styleUrls: ["event.component.scss"],
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class EventComponent implements OnInit {
	protected event$!: Observable<Event[]>;
	protected eventStatus$!: Observable<string>;
	protected eventError$!: Observable<string>;
	protected handsetLayout$!: Observable<boolean>;

	constructor(
		private facadeService: FacadeService,
		private breakpointObserver: BreakpointObserver,
		private eventService: EventService
	) {
	}

	ngOnInit() {
		this.handsetLayout$ = this.breakpointObserver
			.observe([Breakpoints.HandsetPortrait])
			.pipe(map((breakpointState) => breakpointState.matches));
		this.facadeService.loadEventState();
		this.event$ = this.facadeService.getAllEvent();
		this.eventStatus$ = this.facadeService.getEventStatus();
		this.eventError$ = this.facadeService.getEventError();
	}

	public createEvent(): void {
		this.eventService.createEntity();
	}
}
