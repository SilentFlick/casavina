import {Injectable} from "@angular/core";
import {Validators} from "@angular/forms";
import {Event} from "@casavina/feature/models/event";
import {Offer} from "@casavina/feature/models/offer";
import {InputModel} from "@casavina/ui/dynamic-components/form/models/input-model";
import {ListInputModel} from "@casavina/ui/dynamic-components/list-paginator/feature/models/list-input-model";
import {Column} from "@casavina/ui/dynamic-components/table-paginator/feature/models/column";
import {MetadataModelService} from "../../feature/metadata-model.service";

@Injectable()
export class EventModelService extends MetadataModelService<Event> {
	constructor() {
		super();
	}

	public getInputModel(existingEvent?: Event): InputModel {
		const currentDate = new Date();
		const minDate = new Date();
		const maxDate = new Date(
			currentDate.getFullYear() + 10,
			currentDate.getMonth(),
			currentDate.getDate()
		);
		return {
			title: {
				type: "text",
				value: existingEvent?.title || "",
				label: "Titel",
				validators: [Validators.required]
			},
			message: {
				type: "textarea",
				value: existingEvent?.message || "",
				label: "Nachricht",
				validators: [Validators.required]
			},
			expiredAt: {
				type: "date",
				value: existingEvent?.expiredAt || "",
				label: "Abgelaufen am",
				validators: [Validators.required],
				minDate: minDate,
				maxDate: maxDate
			}
		};
	}

	public getListInputModel(locale: string): ListInputModel<Event> {
		return {
			title: (item: Event) => item.title,
			itemProperties: [
				{
					label: "Details",
					icon: "sms",
					value: (item: Event) => item.message
				},
				{
					label: "Erstellt am",
					icon: "calendar_add_on",
					value: (item: Event) =>
						new Date(item.createdAt).toLocaleString(locale)
				},
				{
					label: "Abgelaufen am",
					icon: "calendar_clock",
					value: (item: Event) =>
						new Date(item.expiredAt).toLocaleString(locale, {
							dateStyle: "medium"
						})
				}
			]
		};
	}

	public getTableColumns(locale: string): Column<Event | Offer>[] {
		return [
			{
				columnDef: "title",
				header: "Veranstaltung",
				searchable: true,
				filter: {
					objectPropertyName: "title",
					objectPropertyType: "text"
				},
				cell: (element: Event) => element.title
			},
			{
				columnDef: "message",
				header: "Details",
				searchable: true,
				cell: (element: Event) => element.message,
				filter: {
					objectPropertyName: "message",
					objectPropertyType: "text"
				}
			},
			{
				columnDef: "createdAt",
				header: "Erstellt am",
				filter: {
					objectPropertyName: "createdAt",
					objectPropertyType: "date"
				},
				cell: (element: Event) =>
					new Date(element.createdAt).toLocaleString(locale)
			},
			{
				columnDef: "expiredAt",
				header: "Abgelaufen am",
				filter: {
					objectPropertyName: "expiredAt",
					objectPropertyType: "date"
				},
				cell: (element: Event) =>
					new Date(element.expiredAt).toLocaleString(locale, {
						dateStyle: "medium"
					})
			}
		];
	}
}
