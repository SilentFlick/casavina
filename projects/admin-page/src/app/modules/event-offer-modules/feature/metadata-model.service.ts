import {InputModel} from "@casavina/ui/dynamic-components/form/models/input-model";
import {ListInputModel} from "@casavina/ui/dynamic-components/list-paginator/feature/models/list-input-model";
import {Column} from "@casavina/ui/dynamic-components/table-paginator/feature/models/column";

export abstract class MetadataModelService<T extends Object> {
	public getInputModel(entity?: T): InputModel {
		return {};
	}

	public getListInputModel(locale: string): ListInputModel<T> {
		return {
			title: (_) => "",
			itemProperties: []
		};
	}

	public getTableColumns(locale: string): Column<T>[] {
		return [];
	}
}
