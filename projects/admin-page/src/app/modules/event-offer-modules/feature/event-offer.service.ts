import {InputModel} from "@casavina/ui/dynamic-components/form/models/input-model";

export abstract class EventOfferService {
	public createEntity() {
	}

	public deleteEntityById(id: string, title: string) {
	}

	public editEntity(id: string, inputModel: InputModel) {
	}
}
