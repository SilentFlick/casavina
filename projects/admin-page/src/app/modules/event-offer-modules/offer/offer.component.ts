import {BreakpointObserver, Breakpoints} from "@angular/cdk/layout";
import {AsyncPipe, NgIf} from "@angular/common";
import {ChangeDetectionStrategy, Component} from "@angular/core";
import {MatButtonModule} from "@angular/material/button";
import {MatIconModule} from "@angular/material/icon";
import {Offer} from "@casavina/feature/models/offer";
import {LoadingErrorStatusComponent} from "@casavina/ui/loading-error-status/loading-error-status.component";
import {FacadeService} from "@casavina/utils/facade-store/facade.service";
import {map, Observable} from "rxjs";
import {EventOfferService} from "../feature/event-offer.service";
import {MetadataModelService} from "../feature/metadata-model.service";
import {EventOfferListComponent} from "../ui/event-offer-list/event-offer-list.component";
import {EventOfferTableComponent} from "../ui/event-offer-table/event-offer-table.component";
import {OfferModelService} from "./feature/offer-model.service";
import {OfferService} from "./utils/offer.service";

@Component({
	selector: "casavina-offer",
	standalone: true,
	imports: [
		AsyncPipe,
		LoadingErrorStatusComponent,
		MatButtonModule,
		MatIconModule,
		NgIf,
		EventOfferListComponent,
		EventOfferTableComponent
	],
	providers: [
		OfferService,
		OfferModelService,
		{
			provide: EventOfferService,
			useClass: OfferService
		},
		{
			provide: MetadataModelService,
			useClass: OfferModelService
		}
	],
	templateUrl: "./offer.component.html",
	styleUrls: ["./offer.component.scss"],
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class OfferComponent {
	protected offer$!: Observable<Offer[]>;
	protected offerStatus$!: Observable<string>;
	protected offerError$!: Observable<string>;
	protected handsetLayout$!: Observable<boolean>;

	constructor(
		private facadeService: FacadeService,
		private breakpointObserver: BreakpointObserver,
		private offerService: OfferService
	) {
	}

	ngOnInit() {
		this.handsetLayout$ = this.breakpointObserver
			.observe([Breakpoints.HandsetPortrait])
			.pipe(map((breakpointState) => breakpointState.matches));
		this.facadeService.loadOfferState();
		this.offer$ = this.facadeService.getAllOffer();
		this.offerStatus$ = this.facadeService.getOfferStatus();
		this.offerError$ = this.facadeService.getOfferError();
	}

	public createOffer(): void {
		this.offerService.createEntity();
	}
}
