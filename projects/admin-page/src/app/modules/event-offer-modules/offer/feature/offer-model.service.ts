import {Injectable} from "@angular/core";
import {AbstractControl, ValidationErrors, Validators} from "@angular/forms";
import {Offer} from "@casavina/feature/models/offer";
import {InputModel} from "@casavina/ui/dynamic-components/form/models/input-model";
import {ListInputModel} from "@casavina/ui/dynamic-components/list-paginator/feature/models/list-input-model";
import {Column} from "@casavina/ui/dynamic-components/table-paginator/feature/models/column";
import {MetadataModelService} from "../../feature/metadata-model.service";

@Injectable({
	providedIn: "root"
})
export class OfferModelService extends MetadataModelService<Offer> {
	constructor() {
		super();
	}

	public getInputModel(existingOffer?: Offer): InputModel {
		const currentDate = new Date();
		const minDate = new Date();
		const maxDate = new Date(
			currentDate.getFullYear() + 10,
			currentDate.getMonth(),
			currentDate.getDate()
		);
		return {
			title: {
				type: "text",
				value: existingOffer?.title || "",
				label: "Titel",
				validators: [Validators.required]
			},
			message: {
				type: "textarea",
				value: existingOffer?.message || "",
				label: "Nachricht",
				validators: [Validators.required]
			},
			price: {
				type: "chips",
				value: existingOffer?.price || [],
				label: "Preis",
				validators: [Validators.required, this.priceValidator.bind(this)]
			},
			expiredAt: {
				type: "date",
				value: existingOffer?.expiredAt || "",
				label: "Abgelaufen am",
				validators: [Validators.required],
				minDate: minDate,
				maxDate: maxDate
			}
		};
	}

	public getListInputModel(locale: string): ListInputModel<Offer> {
		return {
			title: (item: Offer) => item.title,
			itemProperties: [
				{
					label: "Details zum Angebot",
					icon: "sms",
					value: (item: Offer) => item.message
				},
				{
					label: "Preise",
					icon: "euro_symbol",
					value: (item: Offer) => item.price.join(", ")
				},
				{
					label: "Erstellt am",
					icon: "calendar_add_on",
					value: (item: Offer) =>
						new Date(item.createdAt).toLocaleString(locale)
				},
				{
					label: "Abgelaufen am",
					icon: "calendar_clock",
					value: (item: Offer) =>
						new Date(item.expiredAt).toLocaleString(locale, {
							dateStyle: "medium"
						})
				}
			]
		};
	}

	public getTableColumns(locale: string): Column<Offer>[] {
		return [
			{
				columnDef: "title",
				header: "Angebot",
				searchable: true,
				filter: {
					objectPropertyName: "title",
					objectPropertyType: "text"
				},
				cell: (element: Offer) => element.title
			},
			{
				columnDef: "message",
				header: "Details",
				searchable: true,
				filter: {
					objectPropertyName: "message",
					objectPropertyType: "text"
				},
				cell: (element: Offer) => element.message
			},
			{
				columnDef: "price",
				header: "Preise",
				searchable: true,
				filter: {
					objectPropertyName: "price",
					objectPropertyType: "text"
				},
				cell: (element: Offer) => element.price.join(", ")
			},
			{
				columnDef: "createdAt",
				header: "Erstellt am",
				filter: {
					objectPropertyName: "createdAt",
					objectPropertyType: "date"
				},
				cell: (element: Offer) =>
					new Date(element.createdAt).toLocaleString(locale)
			},
			{
				columnDef: "expiredAt",
				header: "Abgelaufen am",
				filter: {
					objectPropertyName: "expiredAt",
					objectPropertyType: "date"
				},
				cell: (element: Offer) =>
					new Date(element.expiredAt).toLocaleString(locale, {
						dateStyle: "medium"
					})
			}
		];
	}

	private priceValidator(control: AbstractControl): ValidationErrors | null {
		const prices: number[] = control.value;
		if (
			prices.length > 0 &&
			prices.length < 4 &&
			prices.every((value) => Number(value) >= 0.1)
		) {
			return null;
		}
		return {invalidPrice: true};
	}
}
