import {TestBed} from "@angular/core/testing";

import {OfferModelService} from "./offer-model.service";

describe("OfferModelService", () => {
	let service: OfferModelService;

	beforeEach(() => {
		TestBed.configureTestingModule({});
		service = TestBed.inject(OfferModelService);
	});

	it("should be created", () => {
		expect(service).toBeTruthy();
	});
});
