import {Injectable} from "@angular/core";
import {InputModel} from "@casavina/ui/dynamic-components/form/models/input-model";
import {FacadeService} from "@casavina/utils/facade-store/facade.service";
import {DialogsService} from "@casavina/utils/services/dialogs.service";
import {ConfirmationService} from "primeng/api";
import {EventOfferService} from "../../feature/event-offer.service";
import {OfferModelService} from "../feature/offer-model.service";

@Injectable()
export class OfferService extends EventOfferService {
	constructor(
		private facadeService: FacadeService,
		private dialogService: DialogsService,
		private confirmationService: ConfirmationService,
		private offerModelService: OfferModelService
	) {
		super();
	}

	public createEntity() {
		this.dialogService
			.openFormDialog(
				this.offerModelService.getInputModel(),
				"Wochenangebot Hinzufügen"
			)
			.subscribe((data) => {
				if (data) {
					this.facadeService.createOffer(data);
				}
			});
	}

	public deleteEntityById(id: string, title: string) {
		this.confirmationService.confirm({
			header: "Löschung bestätigen",
			message: `Sind Sie sicher, dass Sie das Wochenangebot '${title}' löschen möchten?`,
			accept: () => {
				this.facadeService.deleteOffer(id);
			},
			reject: () => {
			},
			acceptButtonStyleClass: "p-button-danger"
		});
	}

	public editEntity(id: string, inputModel: InputModel) {
		this.dialogService
			.openFormDialog(inputModel, "Wochenangebot Ändern")
			.subscribe((data) => {
				if (data) {
					this.facadeService.updateOffer({
						...data,
						id: id
					});
				}
			});
	}
}
