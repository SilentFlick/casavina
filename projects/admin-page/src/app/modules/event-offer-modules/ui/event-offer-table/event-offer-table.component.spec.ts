import {ComponentFixture, TestBed} from "@angular/core/testing";

import {EventOfferTableComponent} from "./event-offer-table.component";

describe("EventTableComponent", () => {
	let component: EventOfferTableComponent;
	let fixture: ComponentFixture<EventOfferTableComponent>;

	beforeEach(() => {
		TestBed.configureTestingModule({
			imports: [EventOfferTableComponent]
		});
		fixture = TestBed.createComponent(EventOfferTableComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it("should create", () => {
		expect(component).toBeTruthy();
	});
});
