import {DatePipe, NgIf} from "@angular/common";
import {ChangeDetectionStrategy, Component, Inject, Input, LOCALE_ID, OnInit} from "@angular/core";
import {MatButtonModule} from "@angular/material/button";
import {MatIconModule} from "@angular/material/icon";
import {Event} from "@casavina/feature/models/event";
import {Offer} from "@casavina/feature/models/offer";
import {
	DynamicTablePaginatorComponent
} from "@casavina/ui/dynamic-components/table-paginator/dynamic-table-paginator.component";
import {Column} from "@casavina/ui/dynamic-components/table-paginator/feature/models/column";
import {
	TableActionsDirective
} from "@casavina/ui/dynamic-components/table-paginator/utils/directive/table-actions.directive";
import {EventOfferService} from "../../feature/event-offer.service";
import {MetadataModelService} from "../../feature/metadata-model.service";

@Component({
	selector: "casavina-event-table",
	standalone: true,
	imports: [
		DynamicTablePaginatorComponent,
		MatButtonModule,
		MatIconModule,
		NgIf,
		TableActionsDirective,
		DatePipe
	],
	templateUrl: "./event-offer-table.component.html",
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class EventOfferTableComponent implements OnInit {
	@Input({required: true}) public dataSource: (Event | Offer)[] = [];
	public columns: Column<Event | Offer>[] = [];

	constructor(
		private eventOfferService: EventOfferService,
		private metadataModelService: MetadataModelService<Event | Offer>,
		@Inject(LOCALE_ID) private locale: string
	) {
	}

	ngOnInit() {
		this.columns = this.metadataModelService.getTableColumns(this.locale);
	}

	public createEntity(): void {
		this.eventOfferService.createEntity();
	}

	public deleteEntityById(selection: Event | Offer) {
		this.eventOfferService.deleteEntityById(selection.id, selection.title);
	}

	public updateEntity(selection: Event | Offer) {
		this.eventOfferService.editEntity(
			selection.id,
			this.metadataModelService.getInputModel(selection)
		);
	}
}
