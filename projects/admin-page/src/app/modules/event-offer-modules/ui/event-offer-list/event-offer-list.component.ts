import {NgIf} from "@angular/common";
import {ChangeDetectionStrategy, Component, Inject, Input, LOCALE_ID, OnInit} from "@angular/core";
import {MatButtonModule} from "@angular/material/button";
import {MatIconModule} from "@angular/material/icon";
import {Event} from "@casavina/feature/models/event";
import {Offer} from "@casavina/feature/models/offer";
import {ListInputModel} from "@casavina/ui/dynamic-components/list-paginator/feature/models/list-input-model";
import {ListPaginatorComponent} from "@casavina/ui/dynamic-components/list-paginator/list-paginator.component";
import {EventOfferService} from "../../feature/event-offer.service";
import {MetadataModelService} from "../../feature/metadata-model.service";

@Component({
	selector: "casavina-event-list",
	standalone: true,
	imports: [ListPaginatorComponent, NgIf, MatButtonModule, MatIconModule],
	templateUrl: "./event-offer-list.component.html",
	styleUrls: ["./event-offer-list.component.scss"],
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class EventOfferListComponent implements OnInit {
	@Input({required: true}) public dataSource: (Event | Offer)[] = [];

	protected selectedOptions: (Event | Offer)[] = [];
	protected listInputModel: ListInputModel<Event | Offer> = {
		title: (_) => "",
		itemProperties: []
	};

	constructor(
		private eventOfferService: EventOfferService,
		private metadataModelService: MetadataModelService<Event | Offer>,
		@Inject(LOCALE_ID) private locale: string
	) {
	}

	ngOnInit() {
		this.listInputModel = this.metadataModelService.getListInputModel(
			this.locale
		);
	}

	public createEntity(): void {
		this.eventOfferService.createEntity();
	}

	public updateEntity(selection: Event | Offer) {
		this.eventOfferService.editEntity(
			selection.id,
			this.metadataModelService.getInputModel(selection)
		);
	}

	public deleteEntityById(selection: Event | Offer) {
		this.eventOfferService.deleteEntityById(selection.id, selection.title);
	}
}
