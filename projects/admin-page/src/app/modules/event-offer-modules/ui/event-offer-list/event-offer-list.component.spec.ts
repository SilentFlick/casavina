import {ComponentFixture, TestBed} from "@angular/core/testing";

import {EventOfferListComponent} from "./event-offer-list.component";

describe("EventListComponent", () => {
	let component: EventOfferListComponent;
	let fixture: ComponentFixture<EventOfferListComponent>;

	beforeEach(() => {
		TestBed.configureTestingModule({
			imports: [EventOfferListComponent]
		});
		fixture = TestBed.createComponent(EventOfferListComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it("should create", () => {
		expect(component).toBeTruthy();
	});
});
