import {Route} from "@angular/router";
import {EventEffects, eventFeature} from "@casavina/data/store/Event";
import {OfferEffects, offerFeature} from "@casavina/data/store/Offer";
import {provideEffects} from "@ngrx/effects";
import {provideState} from "@ngrx/store";

export default [
	{
		path: "",
		title: "Casa Vina - Veranstaltungen",
		loadComponent: () =>
			import("./event/event.component").then((c) => c.EventComponent),
		providers: [provideState(eventFeature), provideEffects(EventEffects)]
	},
	{
		path: "offer",
		title: "Casa Vina - Wochenangebot",
		loadComponent: () =>
			import("./offer/offer.component").then((c) => c.OfferComponent),
		providers: [provideState(offerFeature), provideEffects(OfferEffects)]
	}
] as Route[];
