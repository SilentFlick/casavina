import {DatePipe, NgForOf, NgIf, SlicePipe} from "@angular/common";
import {ChangeDetectionStrategy, Component, Input} from "@angular/core";
import {MatButtonModule} from "@angular/material/button";
import {MatCardModule} from "@angular/material/card";
import {MatDividerModule} from "@angular/material/divider";
import {MatIconModule} from "@angular/material/icon";
import {MatListModule} from "@angular/material/list";
import {MatMenuModule} from "@angular/material/menu";
import {MatPaginatorIntl, MatPaginatorModule} from "@angular/material/paginator";
import {Reservation} from "@casavina/feature/models/reservation";
import {
	MatPaginatorInternationalizationService
} from "@casavina/utils/services/mat-paginator-internationalization.service";
import {ReservationService} from "../utils/reservation.service";

@Component({
	selector: "casavina-reservation-feeds",
	standalone: true,
	imports: [
		DatePipe,
		MatButtonModule,
		MatDividerModule,
		MatIconModule,
		MatListModule,
		MatMenuModule,
		NgForOf,
		NgIf,
		MatPaginatorModule,
		SlicePipe,
		MatCardModule
	],
	templateUrl: "./reservation-feeds.component.html",
	styleUrls: ["./reservation-feeds.component.scss"],
	changeDetection: ChangeDetectionStrategy.OnPush,
	providers: [
		{
			provide: MatPaginatorIntl,
			useClass: MatPaginatorInternationalizationService
		}
	]
})
export class ReservationFeedsComponent {
	@Input({required: true}) public dataSource: ReadonlyArray<Reservation> = [];

	constructor(private reservationService: ReservationService) {
	}

	public closeReservation(reservation: Reservation) {
		this.reservationService.closeReservation(reservation);
	}
}
