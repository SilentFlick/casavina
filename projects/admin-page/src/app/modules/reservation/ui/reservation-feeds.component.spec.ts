import {ComponentFixture, TestBed} from "@angular/core/testing";

import {ReservationFeedsComponent} from "./reservation-feeds.component";

describe("ReservationFeedsComponent", () => {
	let component: ReservationFeedsComponent;
	let fixture: ComponentFixture<ReservationFeedsComponent>;

	beforeEach(() => {
		TestBed.configureTestingModule({
			imports: [ReservationFeedsComponent]
		});
		fixture = TestBed.createComponent(ReservationFeedsComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it("should create", () => {
		expect(component).toBeTruthy();
	});
});
