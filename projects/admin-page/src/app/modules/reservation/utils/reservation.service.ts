import {Injectable} from "@angular/core";
import {Reservation} from "@casavina/feature/models/reservation";
import {FacadeService} from "@casavina/utils/facade-store/facade.service";

@Injectable()
export class ReservationService {
	constructor(private facadeService: FacadeService) {
	}

	public closeReservation(reservation: Reservation) {
		this.facadeService.toggleReservationStatus({
			...reservation,
			status: "CLOSED"
		});
	}
}
