import {AsyncPipe, NgIf} from "@angular/common";
import {ChangeDetectionStrategy, Component, OnInit} from "@angular/core";
import {MatButtonModule} from "@angular/material/button";
import {RouterLink} from "@angular/router";
import {Reservation} from "@casavina/feature/models/reservation";
import {LoadingErrorStatusComponent} from "@casavina/ui/loading-error-status/loading-error-status.component";
import {FacadeService} from "@casavina/utils/facade-store/facade.service";
import {Observable} from "rxjs";
import {ReservationFeedsComponent} from "./ui/reservation-feeds.component";
import {ReservationService} from "./utils/reservation.service";

@Component({
	standalone: true,
	selector: "casavina-reservation",
	templateUrl: "./reservation.component.html",
	styleUrls: ["./reservation.component.scss"],
	imports: [
		NgIf,
		AsyncPipe,
		ReservationFeedsComponent,
		LoadingErrorStatusComponent,
		MatButtonModule,
		RouterLink
	],
	providers: [ReservationService],
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class ReservationComponent implements OnInit {
	public reservations$!: Observable<Reservation[]>;
	public reservationStatus$!: Observable<string>;
	public reservationError$!: Observable<string>;

	constructor(private facadeService: FacadeService) {
	}

	ngOnInit(): void {
		this.facadeService.loadReservationState();
		this.reservations$ = this.facadeService.getAllReservation();
		this.reservationStatus$ = this.facadeService.getReservationStatus();
		this.reservationError$ = this.facadeService.getReservationError();
	}
}
