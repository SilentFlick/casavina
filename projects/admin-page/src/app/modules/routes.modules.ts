import {inject} from "@angular/core";
import {Route} from "@angular/router";
import {CoreEffects, CoreReducers} from "@casavina/data/store/Core/core.store";
import {ReservationEffects, reservationFeature} from "@casavina/data/store/Reservation";
import {UsersEffects, usersFeature} from "@casavina/data/store/Users";
import {FacadeService} from "@casavina/utils/facade-store/facade.service";
import {adminGuard} from "@casavina/utils/guards/admin.guard";
import {provideEffects} from "@ngrx/effects";
import {provideState} from "@ngrx/store";

export default [
	{
		path: "",
		title: "Casa Vina - Reservierungen",
		loadComponent: () =>
			import("./reservation/reservation.component").then(
				(c) => c.ReservationComponent
			),
		providers: [
			provideState(reservationFeature),
			provideEffects(ReservationEffects)
		]
	},
	{
		path: "menu",
		loadChildren: () => import("./core-modules/routes.coremodules"),
		providers: [CoreReducers, provideEffects(CoreEffects)],
		resolve: {data: () => inject(FacadeService).loadCoreState()}
	},
	{
		path: "event",
		loadChildren: () =>
			import("./event-offer-modules/routes.eventoffermodules")
	},
	{
		path: "user",
		loadComponent: () =>
			import("./user/user.component").then((c) => c.UserComponent),
		canActivate: [adminGuard],
		providers: [provideState(usersFeature), provideEffects(UsersEffects)]
	}
] as Route[];
