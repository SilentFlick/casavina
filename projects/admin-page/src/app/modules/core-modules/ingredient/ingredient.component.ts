import {AsyncPipe, NgForOf, NgIf} from "@angular/common";
import {ChangeDetectionStrategy, Component, OnInit} from "@angular/core";
import {MatButtonModule} from "@angular/material/button";
import {MatIconModule} from "@angular/material/icon";
import {Ingredient} from "@casavina/feature/models/ingredient";
import {DynamicFormComponent} from "@casavina/ui/dynamic-components/form/dynamic-form.component";
import {
	DynamicTablePaginatorComponent
} from "@casavina/ui/dynamic-components/table-paginator/dynamic-table-paginator.component";
import {Column} from "@casavina/ui/dynamic-components/table-paginator/feature/models/column";
import {
	TableActionsDirective
} from "@casavina/ui/dynamic-components/table-paginator/utils/directive/table-actions.directive";
import {LoadingErrorStatusComponent} from "@casavina/ui/loading-error-status/loading-error-status.component";
import {FacadeService} from "@casavina/utils/facade-store/facade.service";
import {Observable} from "rxjs";
import {getInputModel} from "./feature/InputModel";
import {IngredientService} from "./utils/ingredient.service";

@Component({
	selector: "casavina-ingredient",
	standalone: true,
	imports: [
		DynamicFormComponent,
		MatButtonModule,
		NgForOf,
		AsyncPipe,
		NgIf,
		DynamicTablePaginatorComponent,
		LoadingErrorStatusComponent,
		MatIconModule,
		TableActionsDirective
	],
	templateUrl: "ingredient.component.html",
	styleUrls: ["ingredient.component.scss"],
	changeDetection: ChangeDetectionStrategy.OnPush,
	providers: [IngredientService]
})
export class IngredientComponent implements OnInit {
	public columns: Column<Ingredient>[] = [
		{
			columnDef: "name",
			header: "Zutat",
			searchable: false,
			filter: {
				objectPropertyName: "name",
				objectPropertyType: "text"
			},
			cell: (element: Ingredient) => element.name
		}
	];
	public ingredient$!: Observable<Ingredient[]>;
	public ingredientStatus$!: Observable<string>;
	public ingredientError$!: Observable<string>;

	constructor(
		private facadeService: FacadeService,
		private ingredientService: IngredientService
	) {
	}

	ngOnInit() {
		this.ingredient$ = this.facadeService.getAllIngredient();
		this.ingredientStatus$ = this.facadeService.getIngredientStatus();
		this.ingredientError$ = this.facadeService.getIngredientError();
	}

	public createIngredient() {
		this.ingredientService.createIngredient();
	}

	public deleteIngredient(selection: Ingredient) {
		this.ingredientService.deleteIngredient(selection.id, selection.name);
	}

	public updateIngredient(selection: Ingredient) {
		this.ingredientService.updateIngredient(
			selection.id,
			getInputModel(selection)
		);
	}
}
