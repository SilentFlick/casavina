import {Injectable} from "@angular/core";
import {InputModel} from "@casavina/ui/dynamic-components/form/models/input-model";
import {FacadeService} from "@casavina/utils/facade-store/facade.service";
import {DialogsService} from "@casavina/utils/services/dialogs.service";
import {ConfirmationService} from "primeng/api";
import {getInputModel} from "../feature/InputModel";

@Injectable()
export class IngredientService {
	constructor(
		private facadeService: FacadeService,
		private dialogService: DialogsService,
		private confirmationService: ConfirmationService
	) {
	}

	public createIngredient() {
		this.dialogService
			.openFormDialog(getInputModel(), "Zutat Hinzufügen")
			.subscribe((data) => {
				if (data) {
					this.facadeService.createIngredient(data);
				}
			});
	}

	public updateIngredient(id: number, inputModel: InputModel) {
		this.dialogService
			.openFormDialog(inputModel, "Zutat Ändern")
			.subscribe((data) => {
				if (data) {
					this.facadeService.updateIngredient({
						...data,
						id: id
					});
				}
			});
	}

	public deleteIngredient(id: number, name: string) {
		this.confirmationService.confirm({
			header: "Löschung bestätigen",
			message: `Sind Sie sicher, dass Sie die Zutat '${name}' löschen möchten?`,
			accept: () => {
				this.facadeService.deleteIngredient(id);
			},
			reject: () => {
			},
			acceptButtonStyleClass: "p-button-danger"
		});
	}
}
