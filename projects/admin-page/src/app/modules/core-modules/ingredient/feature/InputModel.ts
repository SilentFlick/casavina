import {Validators} from "@angular/forms";
import {Ingredient} from "@casavina/feature/models/ingredient";
import {InputModel} from "@casavina/ui/dynamic-components/form/models/input-model";

export function getInputModel(existingIngredient?: Ingredient): InputModel {
	return {
		name: {
			type: "text",
			value: existingIngredient?.name || "",
			label: "Zutat",
			validators: [Validators.required]
		}
	};
}
