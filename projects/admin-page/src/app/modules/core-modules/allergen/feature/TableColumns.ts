import {Allergen} from "@casavina/feature/models/allergen";
import {Column} from "@casavina/ui/dynamic-components/table-paginator/feature/models/column";

export function getTableColumns() {
	const result: Column<Allergen>[] = [
		{
			columnDef: "name",
			header: "Name",
			searchable: true,
			filter: {
				objectPropertyName: "name",
				objectPropertyType: "text"
			},
			cell: (element: Allergen) => element.name
		},
		{
			columnDef: "shortName",
			header: "Kurzname",
			searchable: true,
			filter: {
				objectPropertyName: "shortName",
				objectPropertyType: "text"
			},
			cell: (element: Allergen) => element.shortName
		}
	];
	return result;
}
