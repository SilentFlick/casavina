import {Validators} from "@angular/forms";
import {Allergen} from "@casavina/feature/models/allergen";
import {InputModel} from "@casavina/ui/dynamic-components/form/models/input-model";

export function getInputModel(existingAllergen?: Allergen): InputModel {
	return {
		name: {
			type: "text",
			value: existingAllergen?.name || "",
			label: "Name",
			updateOn: "blur",
			validators: [Validators.required]
		},
		shortName: {
			type: "text",
			value: existingAllergen?.shortName || "",
			label: "Kurzname",
			updateOn: "blur",
			validators: [Validators.required]
		}
	};
}
