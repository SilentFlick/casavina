import {Injectable} from "@angular/core";
import {InputModel} from "@casavina/ui/dynamic-components/form/models/input-model";
import {FacadeService} from "@casavina/utils/facade-store/facade.service";
import {DialogsService} from "@casavina/utils/services/dialogs.service";
import {ConfirmationService} from "primeng/api";
import {getInputModel} from "../feature/InputModel";

@Injectable()
export class AllergenService {
	constructor(
		private facadeService: FacadeService,
		private dialogService: DialogsService,
		private confirmationService: ConfirmationService
	) {
	}

	public createAllergen() {
		this.dialogService
			.openFormDialog(getInputModel(), "Allergen Hinzufügen")
			.subscribe((data) => {
				if (data) {
					this.facadeService.createAllergen(data);
				}
			});
	}

	public updateAllergen(id: number, inputModel: InputModel) {
		this.dialogService
			.openFormDialog(inputModel, "Allergen Ändern")
			.subscribe((data) => {
				if (data) {
					this.facadeService.updateAllergen({
						...data,
						id: id
					});
				}
			});
	}

	public deleteAllergen(id: number, name: string) {
		this.confirmationService.confirm({
			header: "Löschung bestätigen",
			message: `Sind Sie sicher, dass Sie das Allergen '${name}' löschen möchten?`,
			accept: () => {
				this.facadeService.deleteAllergen(id);
			},
			reject: () => {
			},
			acceptButtonStyleClass: "p-button-danger"
		});
	}
}
