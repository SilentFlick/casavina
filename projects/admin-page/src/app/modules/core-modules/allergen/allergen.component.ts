import {AsyncPipe, NgIf} from "@angular/common";
import {ChangeDetectionStrategy, Component, OnInit} from "@angular/core";
import {MatButtonModule} from "@angular/material/button";
import {MatIconModule} from "@angular/material/icon";
import {Allergen} from "@casavina/feature/models/allergen";
import {
	DynamicTablePaginatorComponent
} from "@casavina/ui/dynamic-components/table-paginator/dynamic-table-paginator.component";
import {Column} from "@casavina/ui/dynamic-components/table-paginator/feature/models/column";
import {
	TableActionsDirective
} from "@casavina/ui/dynamic-components/table-paginator/utils/directive/table-actions.directive";
import {LoadingErrorStatusComponent} from "@casavina/ui/loading-error-status/loading-error-status.component";
import {FacadeService} from "@casavina/utils/facade-store/facade.service";
import {Observable} from "rxjs";
import {getInputModel} from "./feature/InputModel";
import {getTableColumns} from "./feature/TableColumns";
import {AllergenService} from "./utils/allergen.service";

@Component({
	selector: "casavina-allergen",
	standalone: true,
	imports: [
		AsyncPipe,
		NgIf,
		MatButtonModule,
		MatIconModule,
		DynamicTablePaginatorComponent,
		TableActionsDirective,
		LoadingErrorStatusComponent
	],
	templateUrl: "./allergen.component.html",
	styleUrls: ["./allergen.component.scss"],
	changeDetection: ChangeDetectionStrategy.OnPush,
	providers: [AllergenService]
})
export class AllergenComponent implements OnInit {
	public allergen$!: Observable<Allergen[]>;
	public allergenStatus$!: Observable<string>;
	public allergenError$!: Observable<string>;
	public columns: Column<Allergen>[] = [];

	constructor(
		protected facadeService: FacadeService,
		private allergenService: AllergenService
	) {
	}

	ngOnInit() {
		this.allergen$ = this.facadeService.getAllAllergen();
		this.allergenStatus$ = this.facadeService.getAllergenStatus();
		this.allergenError$ = this.facadeService.getAllergenError();
		this.columns = getTableColumns();
	}

	public createAllergen() {
		this.allergenService.createAllergen();
	}

	public updateAllergen(selection: Allergen) {
		this.allergenService.updateAllergen(selection.id, getInputModel(selection));
	}

	public deleteAllergen(selection: Allergen) {
		this.allergenService.deleteAllergen(selection.id, selection.name);
	}
}
