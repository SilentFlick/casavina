import {Injectable} from "@angular/core";
import {Allergen} from "@casavina/feature/models/allergen";
import {Category} from "@casavina/feature/models/category";
import {Ingredient} from "@casavina/feature/models/ingredient";
import {Menu} from "@casavina/feature/models/menu";
import {FacadeService} from "@casavina/utils/facade-store/facade.service";
import {DialogsService} from "@casavina/utils/services/dialogs.service";
import {ConfirmationService} from "primeng/api";
import {getCreateCategoryInputModel, getMenuInputModel} from "../feature/InputModel";

@Injectable()
export class MenuService {
	constructor(
		private facadeService: FacadeService,
		private dialogService: DialogsService,
		private confirmationService: ConfirmationService
	) {
	}

	public createMenu(
		allergens: Allergen[],
		categories: Category[],
		ingredients: Ingredient[],
		categoryId: number
	) {
		this.dialogService
			.openFormDialog(
				getMenuInputModel(allergens, categories, ingredients, {
					id: 0,
					name: "",
					category: categoryId,
					ingredients: [],
					description: null,
					allergens: [],
					price: [],
					image: null
				}),
				"Menu Hinzufügen"
			)
			.subscribe((data) => {
				if (data) {
					this.facadeService.createMenu(data);
				}
			});
	}

	public updateMenu(
		allergens: Allergen[],
		categories: Category[],
		ingredients: Ingredient[],
		menu: Menu
	) {
		this.dialogService
			.openFormDialog(
				getMenuInputModel(allergens, categories, ingredients, menu),
				"Menu Ändern"
			)
			.subscribe((data) => {
				if (data) {
					this.facadeService.updateMenu(data, menu.id);
				}
			});
	}

	public deleteMenu(id: number, name: string) {
		this.confirmationService.confirm({
			header: "Löschung bestätigen",
			message: `Sind Sie sicher, dass Sie das Menü '${name}' löschen möchten?`,
			accept: () => {
				this.facadeService.deleteMenu(id);
			},
			reject: () => {
			},
			acceptButtonStyleClass: "p-button-danger"
		});
	}

	public createCategory() {
		this.dialogService
			.openFormDialog(getCreateCategoryInputModel(), "Kategorie Hinzufügen")
			.subscribe((data) => {
				if (data) {
					this.facadeService.createCategory(data);
				}
			});
	}

	public deleteCategory(id: number, name: string) {
		this.confirmationService.confirm({
			header: "Löschung bestätigen",
			message: `Sind Sie sicher, dass Sie die Kategorie '${name}' löschen möchten?`,
			accept: () => {
				this.facadeService.deleteCategory(id);
			},
			reject: () => {
			},
			acceptButtonStyleClass: "p-button-danger"
		});
	}
}
