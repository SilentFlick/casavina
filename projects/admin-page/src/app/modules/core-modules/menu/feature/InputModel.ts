import {AbstractControl, ValidationErrors, Validators} from "@angular/forms";
import {Allergen} from "@casavina/feature/models/allergen";
import {Category} from "@casavina/feature/models/category";
import {Ingredient} from "@casavina/feature/models/ingredient";
import {Menu} from "@casavina/feature/models/menu";
import {InputModel} from "@casavina/ui/dynamic-components/form/models/input-model";

export function getMenuInputModel(
	allergens: Allergen[],
	categories: Category[],
	ingredients: Ingredient[],
	existingMenu?: Menu
): InputModel {
	return {
		category: {
			type: "select",
			value: existingMenu?.category || "",
			label: "Kategorie",
			validators: [Validators.required],
			multiple: false,
			options: categories,
			optionLabelName: "name",
			optionValueName: "id"
		},
		name: {
			type: "text",
			value: existingMenu?.name || "",
			label: "Name",
			validators: [Validators.required]
		},
		ingredients: {
			type: "select",
			value: existingMenu?.ingredients || [],
			label: "Zutaten",
			validators: [Validators.required],
			multiple: true,
			options: ingredients,
			optionLabelName: "name",
			optionValueName: "id"
		},
		allergens: {
			type: "select",
			value: existingMenu?.allergens || [],
			label: "Allergene",
			validators: [Validators.required],
			multiple: true,
			options: allergens,
			optionLabelName: "name",
			optionValueName: "id"
		},
		price: {
			type: "chips",
			value: existingMenu?.price || [],
			label: "Preis",
			validators: [Validators.required, priceValidator]
		},
		description: {
			type: "textarea",
			value: existingMenu?.description || "",
			label: "Beschreibung",
			validators: []
		}
	};
}

function priceValidator(control: AbstractControl): ValidationErrors | null {
	const prices: number[] = control.value;
	if (
		prices.length > 0 &&
		prices.length < 4 &&
		prices.every((value) => Number(value) >= 0.1)
	) {
		return null;
	}
	return {invalidPrice: true};
}

export function getCreateCategoryInputModel(): InputModel {
	return {
		name: {
			type: "text",
			value: "",
			label: "Name",
			validators: [Validators.required]
		}
	};
}
