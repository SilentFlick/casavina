import {AsyncPipe, NgIf} from "@angular/common";
import {ChangeDetectionStrategy, Component, OnInit} from "@angular/core";
import {Validators} from "@angular/forms";
import {Allergen} from "@casavina/feature/models/allergen";
import {Category} from "@casavina/feature/models/category";
import {Ingredient} from "@casavina/feature/models/ingredient";
import {DynamicFormComponent} from "@casavina/ui/dynamic-components/form/dynamic-form.component";
import {InputModel} from "@casavina/ui/dynamic-components/form/models/input-model";
import {LoadingErrorStatusComponent} from "@casavina/ui/loading-error-status/loading-error-status.component";
import {FacadeService} from "@casavina/utils/facade-store/facade.service";
import {Observable} from "rxjs";
import {MenuItemsComponent} from "./ui/menu-items/menu-items.component";
import {MenuService} from "./utils/menu.service";

@Component({
	selector: "casavina-menu",
	standalone: true,
	imports: [
		AsyncPipe,
		NgIf,
		DynamicFormComponent,
		MenuItemsComponent,
		LoadingErrorStatusComponent
	],
	templateUrl: "./menu.component.html",
	changeDetection: ChangeDetectionStrategy.OnPush,
	providers: [MenuService]
})
export class MenuComponent implements OnInit {
	public expandExpansionPanel: number = 0;
	public inputModel: InputModel = {
		name: {
			type: "text",
			value: "",
			label: "Kategorie",
			validators: [Validators.required]
		}
	};
	public category$!: Observable<Category[]>;
	public categoryStatus$!: Observable<string>;
	public categoryError$!: Observable<string>;
	public ingredient$!: Observable<Ingredient[]>;
	public allergen$!: Observable<Allergen[]>;

	constructor(protected facadeService: FacadeService) {
	}

	ngOnInit() {
		this.category$ = this.facadeService.getAllCategory();
		this.categoryStatus$ = this.facadeService.getCategoryStatus();
		this.categoryError$ = this.facadeService.getCategoryError();
		this.allergen$ = this.facadeService.getAllAllergen();
		this.ingredient$ = this.facadeService.getAllIngredient();
	}
}
