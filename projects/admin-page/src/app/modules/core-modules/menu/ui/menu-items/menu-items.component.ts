import {AsyncPipe, NgForOf, NgIf} from "@angular/common";
import {ChangeDetectionStrategy, Component, Input} from "@angular/core";
import {MatButtonModule} from "@angular/material/button";
import {MatCardModule} from "@angular/material/card";
import {MatExpansionModule} from "@angular/material/expansion";
import {MatIconModule} from "@angular/material/icon";
import {Allergen} from "@casavina/feature/models/allergen";
import {Category} from "@casavina/feature/models/category";
import {Ingredient} from "@casavina/feature/models/ingredient";
import {Menu} from "@casavina/feature/models/menu";
import {FacadeService} from "@casavina/utils/facade-store/facade.service";
import {MenuService} from "../../utils/menu.service";

@Component({
	selector: "casavina-menu-items",
	standalone: true,
	imports: [
		MatButtonModule,
		MatCardModule,
		MatExpansionModule,
		MatIconModule,
		AsyncPipe,
		NgForOf,
		NgIf
	],
	templateUrl: "./menu-items.component.html",
	styleUrls: ["./menu-items.component.scss"],
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class MenuItemsComponent {
	@Input({required: true}) public categories: Category[] = [];
	@Input({required: true}) public allergens: Allergen[] = [];
	@Input({required: true}) public ingredients: Ingredient[] = [];
	protected expandExpansionPanel: number = 0;

	constructor(
		protected facadeService: FacadeService,
		private menuService: MenuService
	) {
	}

	public updateExistingMenuItem(menu: Menu) {
		this.menuService.updateMenu(
			this.allergens,
			this.categories,
			this.ingredients,
			menu
		);
		this.expandExpansionPanel = menu.category;
	}

	public deleteMenu(menu: Menu) {
		this.menuService.deleteMenu(menu.id, menu.name);
	}

	public createNewMenuItem(id: number) {
		this.menuService.createMenu(
			this.allergens,
			this.categories,
			this.ingredients,
			id
		);
		this.expandExpansionPanel = id;
	}

	public createCategory() {
		this.menuService.createCategory();
	}

	public deleteCategory(category: Category) {
		this.menuService.deleteCategory(category.id, category.name);
	}
}
