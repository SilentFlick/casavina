import {Route} from "@angular/router";

export default [
	{
		path: "",
		title: "Casa Vina - Menü",
		loadComponent: () =>
			import("./menu/menu.component").then((c) => c.MenuComponent)
	},
	{
		path: "allergen",
		title: "Casa Vina - Allergien",
		loadComponent: () =>
			import("./allergen/allergen.component").then((c) => c.AllergenComponent)
	},
	{
		path: "ingredient",
		title: "Casa Vina - Zutaten",
		loadComponent: () =>
			import("./ingredient/ingredient.component").then(
				(c) => c.IngredientComponent
			)
	}
] as Route[];
