import {AsyncPipe, NgIf} from "@angular/common";
import {Component, OnInit} from "@angular/core";
import {Users} from "@casavina/feature/models/users";
import {LoadingErrorStatusComponent} from "@casavina/ui/loading-error-status/loading-error-status.component";
import {FacadeService} from "@casavina/utils/facade-store/facade.service";
import {Observable} from "rxjs";
import {UsersListComponent} from "./ui/users-list/users-list.component";

@Component({
	standalone: true,
	selector: "casavina-user",
	templateUrl: "./user.component.html",
	styleUrls: ["./user.component.scss"],
	imports: [NgIf, AsyncPipe, LoadingErrorStatusComponent, UsersListComponent]
})
export class UserComponent implements OnInit {
	public users$!: Observable<Users[]>;
	public userStatus$!: Observable<string>;
	public userError$!: Observable<string>;

	constructor(private facadeService: FacadeService) {
	}

	ngOnInit() {
		this.facadeService.loadUsers();
		this.users$ = this.facadeService.getAllUsers();
		this.userStatus$ = this.facadeService.getUserStatus();
		this.userError$ = this.facadeService.getUserError();
	}
}
