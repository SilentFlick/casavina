import {NgForOf, NgIf} from "@angular/common";
import {ChangeDetectionStrategy, Component, Input} from "@angular/core";
import {AbstractControl, ValidationErrors, Validators} from "@angular/forms";
import {MatBadgeModule} from "@angular/material/badge";
import {MatButtonModule} from "@angular/material/button";
import {MatCardModule} from "@angular/material/card";
import {MatIconModule} from "@angular/material/icon";
import {MatMenuModule} from "@angular/material/menu";
import {Users} from "@casavina/feature/models/users";
import {InputModel} from "@casavina/ui/dynamic-components/form/models/input-model";
import {FacadeService} from "@casavina/utils/facade-store/facade.service";
import {DialogsService} from "@casavina/utils/services/dialogs.service";
import {ConfirmationService} from "primeng/api";
import {AvatarModule} from "primeng/avatar";

@Component({
	selector: "casavina-users-list",
	standalone: true,
	imports: [
		MatCardModule,
		AvatarModule,
		MatMenuModule,
		MatButtonModule,
		MatIconModule,
		NgForOf,
		NgIf,
		MatBadgeModule
	],
	templateUrl: "./users-list.component.html",
	styleUrls: ["./users-list.component.scss"],
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class UsersListComponent {
	@Input({required: true}) public users: Users[] = [];

	constructor(
		private facadeService: FacadeService,
		private dialogService: DialogsService,
		private confirmationService: ConfirmationService
	) {
	}

	public createUser() {
		const inputModel: InputModel = {
			username: {
				type: "text",
				label: "Benutzername",
				value: "",
				validators: [
					Validators.required,
					Validators.pattern(/^\S*$/),
					Validators.minLength(3),
					Validators.maxLength(30),
					this.usernameValidator.bind(this)
				],
				autocomplete: "username"
			},
			password: {
				type: "password",
				label: "Passwort",
				value: "",
				validators: [
					Validators.required,
					Validators.minLength(8),
					Validators.maxLength(255)
				],
				autocomplete: "new-password",
				feedback: true
			},
			repeatPassword: {
				type: "password",
				label: "Passwort wiederholen",
				value: "",
				validators: [
					Validators.required,
					Validators.minLength(8),
					Validators.maxLength(255)
				],
				autocomplete: "new-password"
			}
		};
		this.dialogService
			.openFormDialog(inputModel, "Benutzer Hinzufügen", [
				this.confirmPasswordValidator.bind(this)
			])
			.subscribe((data) => {
				if (data) {
					this.facadeService.createUser({
						...data,
						enabled: true
					});
				}
			});
	}

	public deleteUser(username: string) {
		this.confirmationService.confirm({
			header: "Löschung bestätigen",
			message: `Wollen Sie wirklich den Benutzer '${username}' löschen?`,
			accept: () => {
				this.facadeService.deleteUser(username);
			},
			reject: () => {
			},
			acceptButtonStyleClass: "p-button-danger"
		});
	}

	public changePassword(username: string) {
		const inputModel: InputModel = {
			oldPassword: {
				type: "password",
				label: "Altes Passwort",
				value: "",
				validators: [
					Validators.required,
					Validators.minLength(8),
					Validators.maxLength(255)
				],
				autocomplete: "current-password"
			},
			newPassword: {
				type: "password",
				label: "Neues Passwort",
				value: "",
				validators: [
					Validators.required,
					Validators.minLength(8),
					Validators.maxLength(255)
				],
				autocomplete: "new-password",
				feedback: true
			},
			repeatPassword: {
				type: "password",
				label: "Passwort wiederholen",
				value: "",
				validators: [
					Validators.required,
					Validators.minLength(8),
					Validators.maxLength(255)
				],
				autocomplete: "new-password"
			}
		};
		this.dialogService
			.openFormDialog(inputModel, "Passwort Verändern", [
				this.confirmPasswordValidator.bind(this)
			])
			.subscribe((data) => {
				if (data) {
					this.facadeService.changePassword({
						...data,
						username: username
					});
				}
			});
	}

	private usernameValidator(control: AbstractControl): ValidationErrors | null {
		return !!this.users.find((user) => user.username === control.value)
			? {invalidUsername: true}
			: null;
	}

	private confirmPasswordValidator(
		control: AbstractControl
	): ValidationErrors | null {
		if (
			control.value.newPassword === control.value.repeatPassword ||
			control.value.password === control.value.repeatPassword
		) {
			return null;
		}
		control.get("repeatPassword")?.setErrors({invalidConfirmPassword: true});
		return {invalidConfirmPassword: true};
	}
}
